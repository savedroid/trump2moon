// tslint:disable
// this is an auto generated file. This will be overwritten

export const getExecution = /* GraphQL */ `
  query GetExecution($id: ID!) {
    getExecution(id: $id) {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      
    }
  }
`;
export const listExecutions = /* GraphQL */ `
  query ListExecutions(
    $filter: ModelExecutionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listExecutions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        twitterId
        status
        transactionId
        amount {
          amount
          currency
          decimals
        }
        createdAt
      }
      nextToken
    }
  }
`;
export const getProfile = /* GraphQL */ `
  query GetProfile($id: ID!) {
    getProfile(id: $id) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      kycLevel
    }
  }
`;
export const listProfiles = /* GraphQL */ `
  query ListProfiles(
    $filter: ModelProfileFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProfiles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        amount {
          amount
          currency
          decimals
        }
        notificationPeriod
        createdAt
        kycLevel
      }
      nextToken
    }
  }
`;
export const getTransaction = /* GraphQL */ `
  query GetTransaction($id: ID!) {
    getTransaction(id: $id) {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      
    }
  }
`;
export const listTransactions = /* GraphQL */ `
  query ListTransactions(
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTransactions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        status
        amount {
          amount
          currency
          decimals
        }
        feeAmount {
          amount
          currency
          decimals
        }
        convertedAmount {
          amount
          currency
          decimals
        }
        createdAt
        executions {
          nextToken
        }
        payment {
          provider
          id
        }
        nexusTransaction
        
      }
      nextToken
    }
  }
`;
export const transactionBy = /* GraphQL */ `
  query TransactionBy(
    $: String
    $sortDirection: ModelSortDirection
    $filter: ModelTransactionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    transactionBy(
      : $
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        status
        amount {
          amount
          currency
          decimals
        }
        feeAmount {
          amount
          currency
          decimals
        }
        convertedAmount {
          amount
          currency
          decimals
        }
        createdAt
        executions {
          nextToken
        }
        payment {
          provider
          id
        }
        nexusTransaction
        
      }
      nextToken
    }
  }
`;
