// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateProfile = /* GraphQL */ `
  subscription OnCreateProfile($owner: String!) {
    onCreateProfile(owner: $owner) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      owner
      kycLevel
    }
  }
`;
export const onDeleteProfile = /* GraphQL */ `
  subscription OnDeleteProfile($owner: String!) {
    onDeleteProfile(owner: $owner) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      owner
      kycLevel
    }
  }
`;
export const onUpdateExecution = /* GraphQL */ `
  subscription OnUpdateExecution {
    onUpdateExecution {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      owner
    }
  }
`;
export const onDeleteExecution = /* GraphQL */ `
  subscription OnDeleteExecution {
    onDeleteExecution {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      owner
    }
  }
`;
export const onCreateTransaction = /* GraphQL */ `
  subscription OnCreateTransaction($owner: String!) {
    onCreateTransaction(owner: $owner) {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          owner
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      owner
    }
  }
`;
export const onDeleteTransaction = /* GraphQL */ `
  subscription OnDeleteTransaction {
    onDeleteTransaction {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          owner
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      owner
    }
  }
`;
export const onUpdateProfile = /* GraphQL */ `
  subscription OnUpdateProfile($owner: String!) {
    onUpdateProfile(owner: $owner) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      owner
      kycLevel
    }
  }
`;
export const onCreateExecution = /* GraphQL */ `
  subscription OnCreateExecution {
    onCreateExecution {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      owner
    }
  }
`;
export const onUpdateTransaction = /* GraphQL */ `
  subscription OnUpdateTransaction {
    onUpdateTransaction {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          owner
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      owner
    }
  }
`;
