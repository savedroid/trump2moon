export const createProfile = /* GraphQL */ `
  mutation CreateProfile(
    $input: CreateProfileInput!
    $condition: ModelProfileConditionInput
  ) {
    createProfile(input: $input, condition: $condition) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      kycLevel
    }
  }
`;

export const createTransaction = /* GraphQL */ `
  mutation CreateTransaction(
    $input: CreateTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    createTransaction(input: $input, condition: $condition) {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
    }
  }
`;

export const deleteExecution = /* GraphQL */ `
  mutation DeleteExecution(
    $input: DeleteExecutionInput!
    $condition: ModelExecutionConditionInput
  ) {
    deleteExecution(input: $input, condition: $condition) {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
    }
  }
`;

export const updateProfile = /* GraphQL */ `
  mutation UpdateProfile(
    $input: UpdateProfileInput!
    $condition: ModelProfileConditionInput
  ) {
    updateProfile(input: $input, condition: $condition) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      kycLevel
    }
  }
`;
