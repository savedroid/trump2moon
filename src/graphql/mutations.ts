// tslint:disable
// this is an auto generated file. This will be overwritten

export const createProfile = /* GraphQL */ `
  mutation CreateProfile(
    $input: CreateProfileInput!
    $condition: ModelProfileConditionInput
  ) {
    createProfile(input: $input, condition: $condition) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      owner
      kycLevel
    }
  }
`;
export const deleteProfile = /* GraphQL */ `
  mutation DeleteProfile(
    $input: DeleteProfileInput!
    $condition: ModelProfileConditionInput
  ) {
    deleteProfile(input: $input, condition: $condition) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      owner
      kycLevel
    }
  }
`;
export const updateExecution = /* GraphQL */ `
  mutation UpdateExecution(
    $input: UpdateExecutionInput!
    $condition: ModelExecutionConditionInput
  ) {
    updateExecution(input: $input, condition: $condition) {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      owner
    }
  }
`;
export const deleteExecution = /* GraphQL */ `
  mutation DeleteExecution(
    $input: DeleteExecutionInput!
    $condition: ModelExecutionConditionInput
  ) {
    deleteExecution(input: $input, condition: $condition) {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      owner
    }
  }
`;
export const createTransaction = /* GraphQL */ `
  mutation CreateTransaction(
    $input: CreateTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    createTransaction(input: $input, condition: $condition) {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          owner
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      owner
    }
  }
`;
export const deleteTransaction = /* GraphQL */ `
  mutation DeleteTransaction(
    $input: DeleteTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    deleteTransaction(input: $input, condition: $condition) {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          owner
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      owner
    }
  }
`;
export const updateProfile = /* GraphQL */ `
  mutation UpdateProfile(
    $input: UpdateProfileInput!
    $condition: ModelProfileConditionInput
  ) {
    updateProfile(input: $input, condition: $condition) {
      id
      amount {
        amount
        currency
        decimals
      }
      notificationPeriod
      createdAt
      owner
      kycLevel
    }
  }
`;
export const createExecution = /* GraphQL */ `
  mutation CreateExecution(
    $input: CreateExecutionInput!
    $condition: ModelExecutionConditionInput
  ) {
    createExecution(input: $input, condition: $condition) {
      id
      twitterId
      status
      transactionId
      amount {
        amount
        currency
        decimals
      }
      createdAt
      owner
    }
  }
`;
export const updateTransaction = /* GraphQL */ `
  mutation UpdateTransaction(
    $input: UpdateTransactionInput!
    $condition: ModelTransactionConditionInput
  ) {
    updateTransaction(input: $input, condition: $condition) {
      id
      status
      amount {
        amount
        currency
        decimals
      }
      feeAmount {
        amount
        currency
        decimals
      }
      convertedAmount {
        amount
        currency
        decimals
      }
      createdAt
      executions {
        items {
          id
          twitterId
          status
          transactionId
          createdAt
          owner
        }
        nextToken
      }
      payment {
        provider
        id
      }
      nexusTransaction
      owner
    }
  }
`;
