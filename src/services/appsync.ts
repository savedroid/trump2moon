import gql from 'graphql-tag'
import AWSAppSyncClient, {AUTH_TYPE} from 'aws-appsync'
import awsconfig from '../../aws-exports'
import Amplify, {Auth} from "aws-amplify"

Amplify.configure(awsconfig)
let client = new AWSAppSyncClient({
    url: awsconfig.aws_appsync_graphqlEndpoint,
    region: awsconfig.aws_appsync_region,
    auth: {
        type: awsconfig.aws_appsync_authenticationType,
        jwtToken : async () => (await Auth.currentSession()).getIdToken().getJwtToken()
    },
    disableOffline : true

});

enum Type {
    mutation, query, none
}

interface GraphqlParams {
    operation : any,
    id? : any ,
    item? : any,
    nextToken? : any,
    paginated? : boolean
}

export class AppSync {

    async do(params : GraphqlParams) {
        const mapping = {
            mutation : Type.mutation,
            query : Type.query
        }
        let operationType : number = Type.none;
        Object.entries(mapping).forEach(([key,type]) => {
            if(params.operation.split(" ").includes(key)) {
                operationType = type
            }
        })
        switch (operationType) {
            case Type.mutation:
                return this.mutate(params)
                break
            case Type.query:
                return this.query(params)
                break
            default:
                throw new Error("Operation type unknown")
        }

    }

    async on(subscription : any, subscriber : any) {
        return client.subscribe({ query : gql(subscription)}).subscribe(subscriber)
    }

    async query(params : GraphqlParams) {
        let all : any = []
        const load = async function(params : GraphqlParams): Promise<any> {
            let result = await client.query({
                query: gql(params.operation),
                fetchPolicy: "network-only",
                variables: {
                    id: params.id,
                    nextToken : params.nextToken,
                    limit : 100
                }
            })
            console.log("Response ",result)
            const returnValue = Object.keys(result.data).length > 0 ? result.data[Object.keys(result.data)[0]] : result
            console.log("appsync, load() response: ", returnValue)
            if(params.paginated )  {
                return returnValue
            } else{
                all.push(...returnValue.items)
                return returnValue.nextToken ? load(Object.assign(params, params, {nextToken : returnValue.nextToken})) : all
            }
        }

        return load(params);
    }

    async mutate(params : GraphqlParams) {
        if(params.id) {
            params.item = { id : params.id }
        }
        let result = await client.mutate({
            mutation: gql(params.operation),
            variables: {
                input: params.item
            }
        });
        if(result.length == 0) throw new Error("could not be mutated")
        result = result.data[Object.keys(result.data)[0]]
        return result.data || result
    }

}
