import * as mutations from "../graphql/customMutations"
import PayPal from "react-native-paypal-wrapper"
import {AppSync} from "./appsync"
import Config from "react-native-config";
import uuid from "react-native-uuid";
import {PaymentProvider, Status} from "./API";
import {Amount} from "../model/Amount";

// Required for Future Payments
const options = {
    merchantName: "savedroid AG",
    merchantPrivacyPolicyUri: "https://google.com",
    merchantUserAgreementUri: "https://google.com",
};

const CLIENT_ID = Config.PAYPAL_CLIENT_ID;
const PAYPAL_ENV = Config.PAYPAL_ENV;

export default class PaymentService {

    private appsync: AppSync;

    constructor(appSync: AppSync) {
        this.appsync = appSync;
    }

    public async startPayment(amount: Amount): Promise<boolean> {
        // 3 env available: NO_NETWORK, SANDBOX, PRODUCTION
        PayPal.initializeWithOptions(PAYPAL_ENV, CLIENT_ID, options)
        console.log("start payment")

        return PayPal.pay<JSON>({
            price: amount.toString(),
            currency: "EUR",
            description: "The amount you saved with Trump Smoove",
        }).then((confirm: { response: { id: any; }; }) => {
            console.log("response is: " + confirm);
            let {id} = confirm.response;
            console.log("transaction id is: " + id);
            return this.doPayment(id, amount);
        }).catch((error: any) => {
            console.log(error);
            return false;
        });
    }

    public async startPayout(amount: Amount, email: String): Promise<boolean> {
        console.log("Payout details", email, amount)
        return this.appsync.do({
            operation: mutations.createTransaction,
            item: {
                amount: amount,
                status: Status.PENDING,
                createdAt: new Date().toISOString(),
                payment: {
                    provider: PaymentProvider.PAYPAL,
                    id: email
                },
                id: uuid.v1(),
            }
        },).then(() => {
            console.log("Payout Success");
            return true;
        }).catch((error) => {
            console.log("Payout Error", error);
            return false;
        });
    }

    private async doPayment(id: string, amount: Amount): Promise<boolean> {
        return this.appsync.do({
            operation: mutations.createTransaction,
            item: {
                amount: amount,
                status: Status.PENDING,
                createdAt: new Date().toISOString(),
                payment: {
                    provider: PaymentProvider.PAYPAL,
                    id: id
                },
                id: uuid.v1(),
            }
        },).then(() => {
            console.log("Success");

            return true;
        }).catch((error) => {
            console.log("Error", error);
            return false;
        });
    }
}
