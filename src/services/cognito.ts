import {Auth} from "aws-amplify";
import awsconfig from "../../aws-exports"
import {Event, Subscribable} from "../helpers/Subscriptions";
import publicIP from "react-native-public-ip";

Auth.configure(awsconfig);

export class CognitoService extends Subscribable {

    static shared = new CognitoService()

    events = {
        sessionEstablished: new Event("sessionEstablished", {}),
        noSession: new Event("noSessionAvailable", {})
    }


    async signIn(parameters: { username: any, password: any }) {
        let {username, password} = parameters;
        const user = await Auth.signIn(username, password);
        return user;
    }

    async forgotPassword(parameters: { username: string }) {
        let {username} = parameters;
        return Auth.forgotPassword(username);
    }

    async forgotPasswordSubmit(parameters: { username: string, code: string, password: string }) {
        let {username, code, password} = parameters;
        return Auth.forgotPasswordSubmit(username, code, password);
    }

    async signUp(parameters: { username: string, password: string }) {
        let {username, password} = parameters;

        let ip = await publicIP();
        let user = await Auth.signUp({
            username,
            password,
            attributes: {
                "email": username,
                "custom:ipOnSignup": ip
            },
        });
        return user;
    }

    confirmSignUp(parameters: { username: string, code: string }) {
        let {username, code} = parameters;
        return Auth.confirmSignUp(username, code)
            .then(Promise.resolve)
            .catch(Promise.reject);
    }

    async resendCode(parameters: { username: string }) {
        return Auth.resendSignUp(parameters.username);
    }

    finishSignIn() {
        this.notify(this.events.sessionEstablished);
    }

    getSession() {
        return Auth.currentSession();
    }

    async signOut() {
        return Auth.signOut({global: true}).then(() => {
            this.notify(this.events.noSession);
            Promise.resolve();
        });
    }
}
