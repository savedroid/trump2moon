/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateProfileInput = {
  id?: string | null,
  amount?: MoneyInput | null,
  notificationPeriod?: NotificationPeriod | null,
  createdAt: string,
  kycLevel?: number | null,
};

export type MoneyInput = {
  amount: number,
  currency: Currency,
  decimals: number,
};

export enum Currency {
  EUR = "EUR",
  USD = "USD",
  BTC = "BTC",
}


export enum NotificationPeriod {
  NONE = "NONE",
  INSTANT = "INSTANT",
  DAILY = "DAILY",
  WEEKLY = "WEEKLY",
}


export type ModelProfileConditionInput = {
  notificationPeriod?: ModelNotificationPeriodInput | null,
  createdAt?: ModelStringInput | null,
  kycLevel?: ModelIntInput | null,
  and?: Array< ModelProfileConditionInput | null > | null,
  or?: Array< ModelProfileConditionInput | null > | null,
  not?: ModelProfileConditionInput | null,
};

export type ModelNotificationPeriodInput = {
  eq?: NotificationPeriod | null,
  ne?: NotificationPeriod | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type DeleteProfileInput = {
  id?: string | null,
};

export type UpdateExecutionInput = {
  id: string,
  twitterId?: string | null,
  status?: Status | null,
  transactionId?: string | null,
  amount?: MoneyInput | null,
  createdAt?: string | null,
  owner?: string | null,
};

export enum Status {
  PENDING = "PENDING",
  CANCELED = "CANCELED",
  DONE = "DONE",
  FAILED = "FAILED",
}


export type ModelExecutionConditionInput = {
  twitterId?: ModelStringInput | null,
  status?: ModelStatusInput | null,
  transactionId?: ModelIDInput | null,
  createdAt?: ModelStringInput | null,
  and?: Array< ModelExecutionConditionInput | null > | null,
  or?: Array< ModelExecutionConditionInput | null > | null,
  not?: ModelExecutionConditionInput | null,
};

export type ModelStatusInput = {
  eq?: Status | null,
  ne?: Status | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type DeleteExecutionInput = {
  id?: string | null,
};

export type CreateTransactionInput = {
  id?: string | null,
  status: Status,
  amount: MoneyInput,
  feeAmount?: MoneyInput | null,
  convertedAmount?: MoneyInput | null,
  createdAt: string,
  payment?: PaymentInput | null,
  nexusTransaction?: string | null,
  owner: string,
};

export type PaymentInput = {
  provider?: PaymentProvider | null,
  id: string,
};

export enum PaymentProvider {
  PAYPAL = "PAYPAL",
}


export type ModelTransactionConditionInput = {
  status?: ModelStatusInput | null,
  createdAt?: ModelStringInput | null,
  nexusTransaction?: ModelStringInput | null,
  and?: Array< ModelTransactionConditionInput | null > | null,
  or?: Array< ModelTransactionConditionInput | null > | null,
  not?: ModelTransactionConditionInput | null,
};

export type DeleteTransactionInput = {
  id?: string | null,
};

export type UpdateProfileInput = {
  id: string,
  amount?: MoneyInput | null,
  notificationPeriod?: NotificationPeriod | null,
  createdAt?: string | null,
  kycLevel?: number | null,
};

export type CreateExecutionInput = {
  id?: string | null,
  twitterId: string,
  status: Status,
  transactionId?: string | null,
  amount: MoneyInput,
  createdAt: string,
  owner: string,
};

export type UpdateTransactionInput = {
  id: string,
  status?: Status | null,
  amount?: MoneyInput | null,
  feeAmount?: MoneyInput | null,
  convertedAmount?: MoneyInput | null,
  createdAt?: string | null,
  payment?: PaymentInput | null,
  nexusTransaction?: string | null,
  owner?: string | null,
};

export type ModelExecutionFilterInput = {
  id?: ModelIDInput | null,
  twitterId?: ModelStringInput | null,
  status?: ModelStatusInput | null,
  transactionId?: ModelIDInput | null,
  createdAt?: ModelStringInput | null,
  owner?: ModelStringInput | null,
  and?: Array< ModelExecutionFilterInput | null > | null,
  or?: Array< ModelExecutionFilterInput | null > | null,
  not?: ModelExecutionFilterInput | null,
};

export type ModelProfileFilterInput = {
  id?: ModelIDInput | null,
  notificationPeriod?: ModelNotificationPeriodInput | null,
  createdAt?: ModelStringInput | null,
  kycLevel?: ModelIntInput | null,
  and?: Array< ModelProfileFilterInput | null > | null,
  or?: Array< ModelProfileFilterInput | null > | null,
  not?: ModelProfileFilterInput | null,
};

export type ModelTransactionFilterInput = {
  id?: ModelIDInput | null,
  status?: ModelStatusInput | null,
  createdAt?: ModelStringInput | null,
  nexusTransaction?: ModelStringInput | null,
  owner?: ModelStringInput | null,
  and?: Array< ModelTransactionFilterInput | null > | null,
  or?: Array< ModelTransactionFilterInput | null > | null,
  not?: ModelTransactionFilterInput | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type CreateProfileMutationVariables = {
  input: CreateProfileInput,
  condition?: ModelProfileConditionInput | null,
};

export type CreateProfileMutation = {
  createProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type DeleteProfileMutationVariables = {
  input: DeleteProfileInput,
  condition?: ModelProfileConditionInput | null,
};

export type DeleteProfileMutation = {
  deleteProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type UpdateExecutionMutationVariables = {
  input: UpdateExecutionInput,
  condition?: ModelExecutionConditionInput | null,
};

export type UpdateExecutionMutation = {
  updateExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type DeleteExecutionMutationVariables = {
  input: DeleteExecutionInput,
  condition?: ModelExecutionConditionInput | null,
};

export type DeleteExecutionMutation = {
  deleteExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type CreateTransactionMutationVariables = {
  input: CreateTransactionInput,
  condition?: ModelTransactionConditionInput | null,
};

export type CreateTransactionMutation = {
  createTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};

export type DeleteTransactionMutationVariables = {
  input: DeleteTransactionInput,
  condition?: ModelTransactionConditionInput | null,
};

export type DeleteTransactionMutation = {
  deleteTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};

export type UpdateProfileMutationVariables = {
  input: UpdateProfileInput,
  condition?: ModelProfileConditionInput | null,
};

export type UpdateProfileMutation = {
  updateProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type CreateExecutionMutationVariables = {
  input: CreateExecutionInput,
  condition?: ModelExecutionConditionInput | null,
};

export type CreateExecutionMutation = {
  createExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type UpdateTransactionMutationVariables = {
  input: UpdateTransactionInput,
  condition?: ModelTransactionConditionInput | null,
};

export type UpdateTransactionMutation = {
  updateTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};

export type GetExecutionQueryVariables = {
  id: string,
};

export type GetExecutionQuery = {
  getExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type ListExecutionsQueryVariables = {
  filter?: ModelExecutionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListExecutionsQuery = {
  listExecutions:  {
    __typename: "ModelExecutionConnection",
    items:  Array< {
      __typename: "Execution",
      id: string,
      twitterId: string,
      status: Status,
      transactionId: string | null,
      amount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      },
      createdAt: string,
      owner: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetProfileQueryVariables = {
  id: string,
};

export type GetProfileQuery = {
  getProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type ListProfilesQueryVariables = {
  filter?: ModelProfileFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListProfilesQuery = {
  listProfiles:  {
    __typename: "ModelProfileConnection",
    items:  Array< {
      __typename: "Profile",
      id: string,
      amount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      } | null,
      notificationPeriod: NotificationPeriod | null,
      createdAt: string,
      owner: string | null,
      kycLevel: number | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetTransactionQueryVariables = {
  id: string,
};

export type GetTransactionQuery = {
  getTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};

export type ListTransactionsQueryVariables = {
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTransactionsQuery = {
  listTransactions:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: Status,
      amount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      },
      feeAmount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      } | null,
      convertedAmount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      } | null,
      createdAt: string,
      executions:  {
        __typename: "ModelExecutionConnection",
        nextToken: string | null,
      } | null,
      payment:  {
        __typename: "Payment",
        provider: PaymentProvider | null,
        id: string,
      } | null,
      nexusTransaction: string | null,
      owner: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type TransactionByOwnerQueryVariables = {
  owner?: string | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTransactionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TransactionByOwnerQuery = {
  transactionByOwner:  {
    __typename: "ModelTransactionConnection",
    items:  Array< {
      __typename: "Transaction",
      id: string,
      status: Status,
      amount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      },
      feeAmount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      } | null,
      convertedAmount:  {
        __typename: "Money",
        amount: number,
        currency: Currency,
        decimals: number,
      } | null,
      createdAt: string,
      executions:  {
        __typename: "ModelExecutionConnection",
        nextToken: string | null,
      } | null,
      payment:  {
        __typename: "Payment",
        provider: PaymentProvider | null,
        id: string,
      } | null,
      nexusTransaction: string | null,
      owner: string,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateProfileSubscriptionVariables = {
  owner: string,
};

export type OnCreateProfileSubscription = {
  onCreateProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type OnDeleteProfileSubscriptionVariables = {
  owner: string,
};

export type OnDeleteProfileSubscription = {
  onDeleteProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type OnUpdateExecutionSubscription = {
  onUpdateExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type OnDeleteExecutionSubscription = {
  onDeleteExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type OnCreateTransactionSubscriptionVariables = {
  owner: string,
};

export type OnCreateTransactionSubscription = {
  onCreateTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};

export type OnDeleteTransactionSubscription = {
  onDeleteTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};

export type OnUpdateProfileSubscriptionVariables = {
  owner: string,
};

export type OnUpdateProfileSubscription = {
  onUpdateProfile:  {
    __typename: "Profile",
    id: string,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    notificationPeriod: NotificationPeriod | null,
    createdAt: string,
    owner: string | null,
    kycLevel: number | null,
  } | null,
};

export type OnCreateExecutionSubscription = {
  onCreateExecution:  {
    __typename: "Execution",
    id: string,
    twitterId: string,
    status: Status,
    transactionId: string | null,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    createdAt: string,
    owner: string,
  } | null,
};

export type OnUpdateTransactionSubscription = {
  onUpdateTransaction:  {
    __typename: "Transaction",
    id: string,
    status: Status,
    amount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    },
    feeAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    convertedAmount:  {
      __typename: "Money",
      amount: number,
      currency: Currency,
      decimals: number,
    } | null,
    createdAt: string,
    executions:  {
      __typename: "ModelExecutionConnection",
      items:  Array< {
        __typename: "Execution",
        id: string,
        twitterId: string,
        status: Status,
        transactionId: string | null,
        createdAt: string,
        owner: string,
      } | null > | null,
      nextToken: string | null,
    } | null,
    payment:  {
      __typename: "Payment",
      provider: PaymentProvider | null,
      id: string,
    } | null,
    nexusTransaction: string | null,
    owner: string,
  } | null,
};
