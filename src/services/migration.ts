import { NativeModules } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

export class MigrationService {

    async migrate() {
        try {

            let checked = await AsyncStorage.getItem("@migrated");
            if (checked) {
                return this.checkExisting();
            }

            let item = await this.checkExisting();
            if (!item) {

                const migrationManager = NativeModules.MigrationManager;
                let email = await migrationManager.migrateExistingUser();

                if (email || "".length > 0) {
                    this.saveExisting(email.toString());
                }
                else {
                    email = null;
                }
                item = email;
            }
            await AsyncStorage.setItem("@migrated", "true");
            return item;
        }

        catch(e) {
            console.error(e);
        }
    }

    checkExisting() {
        return AsyncStorage.getItem("@lastKnownUser");
    }

    saveExisting(email:string) {
        return AsyncStorage.setItem("@lastKnownUser", email);
    }
}
