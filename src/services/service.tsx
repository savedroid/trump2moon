import {AppSync} from "./appsync"
import {listProfiles} from "../graphql/queries";
import {NotificationPeriod} from "./API";
import {createProfile, updateProfile} from "../graphql/customMutations";
import {Amount} from "../model/Amount";

export default class SavingRuleService {

    private appsync: AppSync;

    constructor(appSync: AppSync) {
        this.appsync = appSync;
    }

    async saveAmount(newAmount: Amount): Promise<boolean> {
        const profile = await this.getProfile();
        console.log("Profile = ", profile);
        if (profile == null) {
            return await this.createProfile(newAmount)
        } else {
            return await this.updateProfile(profile, newAmount)
        }
    }

    private async updateProfile(profile: any, newAmount: Amount): Promise<boolean> {
        return this.appsync.do({
            operation: updateProfile,
            item: {
                id: profile.id,
                amount: newAmount,
                notificationPeriod: profile.notificationPeriod,
                createdAt: new Date().toISOString(),
            }
        },).then((updatedProfile) => {
            console.log("updatedProfile() Success = ", updatedProfile)
            return true;
        }).catch((error) => {
            console.log("updateProfile() Error = ", error);
            return false;
        });
    }

    private async getProfile(): Promise<any> {
        return this.appsync.do({
            operation: listProfiles,
            paginated: true,
        },).then((profiles) => {
            console.log("getProfile() = ", profiles);
            return profiles.items[0];
        }).catch((error) => {
            console.log("getProfile() Error = ", error);
            return null;
        });
    }

    /**
     * Creates a profile for the user if it does not exist yet.
     */
    private async createProfile(newAmount: Amount): Promise<boolean> {
        return this.appsync.do({
            operation: createProfile,
            item: {
                amount: newAmount,
                notificationPeriod: NotificationPeriod.INSTANT,
                createdAt: new Date().toISOString(),
            }
        },).then((createdProfile) => {
            console.log("Created profile = ", createdProfile)
            return true;
        }).catch((error) => {
            console.log("createProfile() Error = ", error)
            return false;
        });
    }
}
