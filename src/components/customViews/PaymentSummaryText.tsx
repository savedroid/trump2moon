import {StyleSheet, Text, View} from "react-native";
import React from "react";

export class PaymentSummaryText extends React.Component<{ title: string, detailText: string }> {
    render() {
        let {title, detailText} = this.props;
        console.log("props", title, detailText)
        return (
            <View style={style.container}>
                <Text style={style.title}>{title}</Text>
                <Text style={style.detail}>{detailText}</Text>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        height: 50,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center"
    },
    title: {
        color: "rgba(0, 0, 0, 0.64)",
        fontSize: 16
    },
    detail: {
        color: "rgba(0, 0, 0, 0.88)",
        fontSize: 16,
        fontWeight: "600"
    },
})
