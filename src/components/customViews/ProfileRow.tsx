import {StyleSheet, Text, View, Image} from "react-native";
import React from "react";

export class ProfileRow extends React.Component<{ text: string, onPressAction: ()=>void, isArrowVisible: boolean }> {
    render() {
        return (
            <View style={style.row} onTouchEnd={ this.props.onPressAction }>
                <Text style={style.text}>{this.props.text}</Text>
                { this.props.isArrowVisible && 
                    <Image style={style.chevron} source={require("../../../resources/chevron_small.png")} />
                }
            </View>
        )
    }
}

const style = StyleSheet.create({
    row: {
        borderTopWidth: 1,
        borderColor: "#A9A9A9",
        alignItems: "center",
        height: 60,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    text: {
        marginStart: 10,
        fontSize: 16,
        fontWeight:"bold",
    },
    chevron: {
        alignContent: "flex-end",
        marginRight: 20
    }
})
