import React from "react";
import {TextInput, View, TouchableOpacity, StyleSheet} from "react-native";
import {Icon} from "react-native-elements";

interface Props {
    placeholder: string,
    returnKeyType: string,
    value: any,
    onChangeText: any
}

interface State {
    hidePassword: boolean
}

export class PasswordTextBox extends React.Component<Props, State> {
    state = {
        hidePassword: true
    };

    render() {

        let {placeholder, returnKeyType, value, onChangeText} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.textBoxContainer}>
                    <TextInput
                        placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                        style={styles.textBox}
                        value={value}
                        underlineColorAndroid="transparent"
                        placeholder={placeholder}
                        secureTextEntry={this.state.hidePassword}
                        autoCompleteType={"password"}
                        autoCorrect={false}
                        onChangeText={onChangeText}
                        returnKeyType={returnKeyType}
                    />
                    <TouchableOpacity activeOpacity={0.8} style={styles.touchableButton}
                                      onPress={this.changePasswordVisibility}>
                        <Icon color={"rgba(0, 0, 0, 0.5)"} type="font-awesome"
                              name={(this.state.hidePassword) ? "eye-slash" : "eye"}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    private changePasswordVisibility = () => {
        this.setState({hidePassword: !this.state.hidePassword});
    }
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            width: "90%",
            marginLeft: "5%",
        },
        textBoxContainer: {
            position: "relative",
            alignSelf: "stretch",
            justifyContent: "center",
            color: "black"
        },
        textBox: {
            fontSize: 14,
            alignSelf: "stretch",
            height: 42,
            paddingRight: 45,
            paddingVertical: 0,
            color: "rgba(0, 0, 0, 1)",
            borderWidth: 1,
            borderBottomColor: "rgba(0, 0, 0, 0.64)",
            borderLeftColor: "transparent",
            borderTopColor: "transparent",
            borderRightColor: "transparent",
        },
        touchableButton: {
            position: "absolute",
            right: 4,
            width: 40,
            padding: 8,
        },
    });

export default PasswordTextBox;