import {StyleSheet, Text, View} from "react-native";
import React from "react";

export class PersonalDetailText extends React.Component<{ text: string }> {
    render() {
        let {text} = this.props;
        return (
            <View style={style.viewStyle}>
                <Text style={style.textInput}>{text}</Text>
            </View>
        )
    }
}

const style = StyleSheet.create({
    viewStyle: {
        borderBottomWidth: 1,
        borderColor: '#A9A9A9',
    },
    textInput: {
        alignSelf: 'stretch',
        height: 40,
        marginTop: 30,
        marginStart: 10,
        fontSize: 16
    },
})