import {Component} from "react";
import * as React from "react";
import {ActivityIndicator, View} from "react-native";


class LoadingSpinner extends Component{
    render(): React.ReactNode {
        return (
            <View style={{justifyContent:"center", height : "100%", width : "100%", flex : 1}}>
                <ActivityIndicator size="large" color="rgba(0, 0, 0, 0.64)" />
            </View>
        )
    }
}
export default LoadingSpinner
