import {default as React} from "react"
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native"

const ConfirmationView = (props: any) => {
    let {title, message, buttonText} = props
    const onPress = () => props.onTouch()
    return (
        <View style={styles.content}>
            <View style={{alignItems: "center"}}>
                <Image style={styles.image}/>
                <Text style={styles.title}>{title} </Text>
                <Text style={styles.message}> {message}</Text>
            </View>

            <TouchableOpacity style={styles.button} onPress={() => {
                onPress()
            }}><Text style={styles.buttonText}>{buttonText}</Text></TouchableOpacity>
        </View>)
}

const styles = StyleSheet.create({
    content: {
        justifyContent: "space-between",
        alignItems: "center",
        height: "100%"
    },
    image: {
        width: 150,
        height: 150,
        marginTop: 20,
        borderRadius: 150 / 2,
        overflow: "hidden",
        borderWidth: 2,
        backgroundColor: "#C4C4C4"
    },
    title: {
        fontSize: 22,
        marginTop: 30,
        textAlign: "center"
    },
    message: {
        fontSize: 16,
        marginTop: 10,
        marginStart: 30,
        marginEnd: 30,
        textAlign: "center"
    },
    button: {
        backgroundColor: "#00BCFE",
        height: 40,
        width: "100%",
        marginBottom: 20,
        borderRadius: 10,
        paddingTop: 12,
    },
    buttonText: {
        color: "#fff",
        textAlign: "center",
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
        fontWeight: "600"
    },
});


export default ConfirmationView
