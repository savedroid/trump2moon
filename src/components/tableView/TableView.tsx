import React, {Component} from 'react';
import {StyleSheet, FlatList, Alert, View} from 'react-native';
import PendingListItem, {Separator} from './TableListItem'

interface PendingData {
    data: any
    isCompletedTransaction: boolean
    onItemDelete: any
}

interface State {
    data: any
    isCompletedTransaction :boolean
}

class TransactionListView extends Component<PendingData, State> {
    state:State ={
        data :this.props.data,
        isCompletedTransaction: this.props.isCompletedTransaction

    }
    keyExtractor = (item: any, index: any) => index.toString()

    onPress = (item) => {
        this.props.onItemDelete(item)
    }

    renderItem = ({item}) => (
        <PendingListItem
            item={item}
            onSwipeFromRight={() => {
            }}
            pressOnRight={() =>
                this.onPress(item)
            }
            isCompletedTransaction={this.state.isCompletedTransaction}
        />
    )

    render() {
        console.log("Data inside main list", this.props.data)
        return (
            <View>
            <FlatList style={styles.list}
                      keyExtractor={this.keyExtractor}
                      data={this.props.data}
                      renderItem={this.renderItem}
                      ItemSeparatorComponent={() => <Separator/>}
            />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    list: {
        width: "100%",

        marginBottom: 50,
    }
});

export default TransactionListView
