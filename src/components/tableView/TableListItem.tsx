import React from "react";
import {StyleSheet, View, Text, Animated, TouchableOpacity, Image} from "react-native";
import Swipeable from "react-native-gesture-handler/Swipeable";
import moment from "moment";
import {Amount} from "../../model/Amount";

export const Separator = () => <View style={styles.separator}/>;

const PendingListItem = ({item, onSwipeFromRight, pressOnRight, isCompletedTransaction}) =>
    <View>
        {!isCompletedTransaction &&
        <View>
            <Swipeable
                renderRightActions={(progress,
                    dragX) => <RightActions dragX={dragX}
                    onPress={pressOnRight}/>}
                onSwipeableRightOpen={onSwipeFromRight}
            >
                <ViewHolder item={item} isCompletedTransaction={isCompletedTransaction}/>
            </Swipeable></View>}{isCompletedTransaction &&
    <View><ViewHolder item={item} isCompletedTransaction={isCompletedTransaction}/></View>
        }
    </View>;

interface ItemDetails {
    title: string,
    type: string
}

const ViewHolder = (props: { item: any, isCompletedTransaction: boolean }) => {
    let {item, isCompletedTransaction} = props;
    //Use this method when balance is converted to set title
    const getTitleForTransaction = (): ItemDetails => Amount.parse(item.amount).lt(0)  ? {
        title: "Payout",
        type: "-"
    } : {title: "Funding", type: ""};

    let itemDetails: ItemDetails = isCompletedTransaction ? getTitleForTransaction() : {
        title: "Donald Trump tweeted",
        type: ""
    };
    console.log(item)
    const amount = Amount.parse(item.amount)
    const converted = item.convertedAmount ? Amount.parse(item.convertedAmount) : Amount.zero
    return (<View style={styles.container}>
        <View style={styles.icon}>
            <Image style={styles.clockIcon} source={require("../../../resources/clock_icon.png")}/>
        </View>
        <View style={styles.h1}>
            <Text style={styles.text1}>{itemDetails.title}</Text>
            <Text>{moment(item.createdAt).format( "D.MM.YYYY HH:MM")
            }</Text>
        </View>
        <View style={styles.h2}>
            <Text style={styles.text3}>{itemDetails.type + "" + (amount.lt(0) ? converted.toString(true) : amount.toString(true))}</Text>
            <Text style={styles.text3}>({amount.lt(0) ? amount.toString(true) : converted.toString(true)})</Text>
        </View>
    </View>
    );
};

const RightActions = ({dragX, onPress}) => {
    const scale = dragX.interpolate({
        inputRange: [-100, 0],
        outputRange: [0.5, 0.2],
        extrapolate: "clamp"
    });

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.rightAction}>
                <Animated.Image style={[styles.rightTrash, {transform: [{scale}]}]}
                    source={require("../../../resources/trash.png")}/>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({

    rightAction: {
        backgroundColor: "#ff0000",
        justifyContent: "center",
        alignItems: "flex-end",
        height: 70,
        width: 100
    },

    clockIcon: {
        width: 21,
        height: 21
    },

    rightTrash: {
        width: 30,
        alignSelf:"center",
        height: 30,
    },

    container: {
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        borderBottomRightRadius:18,
        borderTopEndRadius:8,
        marginEnd:-10,
        paddingEnd:20,
        borderBottomEndRadius: 18,
        height: 70,
    },

    icon: {
        backgroundColor: "#EEECEC",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 22.5,
        opacity: 0.6,
        width: 45,
        height: 45
    },

    separator: {
        flex: 1,
        height: 1,
        backgroundColor: "#EEECEC",

    },

    h1: {
        padding: 12,
    },

    h2: {
        flex: 1,
        justifyContent: "flex-end"

    },

    text1: {
        paddingBottom: 5,
        color: "#000",
        opacity: 0.4,
        fontSize: 16
    },

    text2: {
        color: "#000",
        opacity: 0.8,
        fontSize: 16
    },

    text3: {
        color: "#000",
        textAlign: "right",
        opacity: 0.8,
        fontSize: 16,
        fontWeight: "600"
    }
});

export default PendingListItem;
