import {Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React from "react";

const CardTweetAmount = (props: any) => {
    let {tweetAmount} = props;
    const onPress = () => {
        console.log("OnPresss", "clicked")
        props.editSavingRule()
    }
    return (<TouchableOpacity style={styles.cardEditAmount} onPress={onPress}>
        <ImageBackground style={styles.image}><Image
            style={{width: 25, height: 25, alignSelf: "center"}}
            source={require("../../resources/launch_icon.png")}/></ImageBackground>
        <View style={{margin: 10, flex: 1}}>
            <Text style={{fontSize: 16}}>When D. Trump tweets convert {tweetAmount.toString(true)} into
                Bitcoin.</Text>
            <Text style={{
                alignSelf: "flex-end", color: "#D90080", fontSize: 14, fontWeight: "bold",
            }}>EDIT</Text>

        </View>
    </TouchableOpacity>)
}

const styles = StyleSheet.create({
    cardEditAmount: {
        padding: 14,
        marginTop: 10,
        borderRadius: 15,
        marginStart: 20,
        marginEnd: 20,
        backgroundColor: "white",
        flexDirection: "row",
        alignContent: "center",
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2
    },
    image: {
        width: 40,
        height: 40,
        marginTop: 10,
        alignSelf: "flex-start",
        backgroundColor: "rgba(238, 236, 236, 0.6)",
        borderRadius: 150 / 2,
        overflow: "hidden",
        justifyContent: "center",
    },
})
export default CardTweetAmount
