import {default as React} from "react";
import {Image, Text, TouchableOpacity} from "react-native";

const PendingBalanceView = (props:any) => {
    let {pendingBalance} = props;
    const onPress = () => props.onTouch();
    return (<TouchableOpacity style={{
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        height:29,
        padding: 4,
        borderRadius: 30,
        alignSelf:"center",
        marginTop: 10,
        borderWidth: 2,
        borderColor: "#D90080"
    }} onPress={onPress}>
        <Image style={{width: 18, height: 18}} source={require("../../resources/pending_balance.png")}/>
        <Text style={{color: "#D90080", fontSize: 14, marginStart: 5,
        }}>{pendingBalance.toString(true)} pending </Text>
    </TouchableOpacity>
    );
};

export default PendingBalanceView;
