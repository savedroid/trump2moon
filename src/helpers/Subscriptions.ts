class Event {
    type : string = ""
    payload = {}

    constructor(type : string, payload : any) {
        this.type = type
        this.payload = payload
    }
}

export interface Subscriber {
    on(event : Event) : void
}

class Subscribable{
    subscribers : Array<Subscriber> = []

    notify(event : Event) {
        console.log(this.subscribers)
        this.subscribers.forEach(s => s.on(event))
    }
    subscribe(subscriber : Subscriber) {
        this.subscribers.push(subscriber)
    }

    subscribeWith(func : (event : Event) => void) {
        this.subscribers.push(new SubscriperWrapper(func))
    }

}

class SubscriperWrapper implements Subscriber {

    callback : any = null

    constructor(callback : any) {
        this.callback = callback
    }

    on(event: Event): void {
        this.callback(event)
    }
}

class Subscriptions extends Subscribable{

    static shared = new Subscriptions()


}

export {Event, Subscribable, Subscriptions}
