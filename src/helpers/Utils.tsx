export const maskEmail = (email: string) => {
    let strLength = email.length;
    return email.substr(0, 2)
        + "*****"
        + email.substr(strLength - 2, strLength);
}