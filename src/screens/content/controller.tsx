import {Component} from "react";
import * as React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import ProfileController from "../profile/main/controller";
import {SettingsController} from "../settings/controller";
import DashboardController from "../dashboard/controller";

const Tab = createBottomTabNavigator();

class ContentController extends Component {


    render(): React.ReactNode {
        return (   <NavigationContainer independent={true}>
        <Tab.Navigator>
            <Tab.Screen name="Dashboard" component={DashboardController} />
            <Tab.Screen name="Profile" component={ProfileController} />
            <Tab.Screen name="Settings" component={SettingsController} />
        </Tab.Navigator>
        </NavigationContainer>)
    }
}

export default ContentController
