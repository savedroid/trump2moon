import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content: {
        justifyContent: "space-between",
        flexGrow: 1,
        height: "100%",
        alignContent:"center",
        margin: 10
    },
    balance: {
        fontSize: 28,
        marginTop: 10,
        textAlign: "center"
    },
    bitcoinBalance: {
        fontSize: 22,
        marginTop: 10,
        textAlign: "center"
    },
    button: {
        backgroundColor: "#00BCFE",
        height: 42,
        marginBottom: 20,
        marginTop:5,
        marginStart:10,
        marginEnd:10,
        borderRadius: 10,
        paddingTop: 12,
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
        fontWeight: "600"
    },
    buttonPayout: {
        borderColor: "#00BCFE",
        height: 42,
        marginLeft: "5%",
        marginBottom: 20,
        borderRadius: 10,
        paddingTop: 12
    },
    container: {
        width: "100%",
        height: 50,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center"
    },
    title: {
        color: "rgba(0, 0, 0, 0.64)",
        fontWeight: "bold",
        fontSize: 16
    },
    detail: {
        color: "rgba(0, 0, 0, 0.88)",
        fontSize: 16,
        fontWeight: "bold"
    },
});

