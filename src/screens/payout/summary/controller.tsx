import React, {Component} from "react";
import {Alert, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";
import {CognitoService} from "../../../services/cognito";
import ConfirmationView from "../../../components/ConfirmationView";
import {PaymentSummaryText} from "../../../components/customViews/PaymentSummaryText";
import {AppSync} from "../../../services/appsync";
import PaymentService from "../../../services/paymentService";
import {Amount} from "../../../model/Amount";

interface Props {
    navigation: any,
    route: any
}

interface State {
    payoutAmount: Amount,
    paypalEmail: string,
    exchangeRate: string,
    transactionFee: string,
    payoutFee: string,
    error: string,
    expectedPayoutAmount: Amount,
    isPayoutDone: boolean,
}

class PayoutSummaryController extends Component<Props, State> {
    state: State = {
        payoutAmount: this.props.route.params.payoutAmount,
        paypalEmail: this.props.route.params.paypalEmail,
        exchangeRate: "",
        transactionFee: "",
        payoutFee: "",
        error: "",
        expectedPayoutAmount: this.props.route.params.payoutAmount,
        isPayoutDone: false,
    }
    private paymentService = new PaymentService(new AppSync());

    private payout = async () => {
        let result = await this.paymentService.startPayout(this.state.expectedPayoutAmount, this.state.paypalEmail)
        if(result){
            this.setState({
                isPayoutDone: true
            });
        }else {
            Alert.alert("Payout failed")
        }
    }

    render() {
        return (
            <View style={{flex:1}}>
                {this.state.isPayoutDone &&
                <View style={{margin: 20}}>
                    <ConfirmationView title="Payout done" message="Payout Done" buttonText="Done"
                        onTouch={this.payout.bind(this)}/>
                </View>
                }
                {!this.state.isPayoutDone &&
                <View style={styles.content}>
                    <View style={{flex: 1, width: "90%", marginStart: 20,marginEnd:20}}>
                        <PaymentSummaryText title={"Selected amount"} detailText={this.state.payoutAmount.toString(true)}/>
                        <PaymentSummaryText title={"Paypal account"} detailText={this.state.paypalEmail}/>
                        <PaymentSummaryText title={"Exchange rate"} detailText={this.state.exchangeRate}/>
                        <PaymentSummaryText title={"Transaction fee"} detailText={this.state.transactionFee}/>
                        <PaymentSummaryText title={"Payout fee"} detailText={this.state.payoutFee}/>
                        <View style={{
                            width: "120%",
                            height: 1,
                            marginTop: 10,
                            backgroundColor: "rgba(204, 204, 204, 0.8)"
                        }}/>

                        <View style={styles.container}>
                            <Text style={styles.title}>EXPECTED PAYOUT AMOUNT</Text>
                            <Text style={styles.detail}>{this.state.expectedPayoutAmount.toString(true)}</Text>
                        </View>
                    </View>
                    <View>
                    <Text style={{marginStart:14, marginEnd:14, fontSize: 14,marginBottom:25, color: "#000000"}}>The displayed exchange rates are not
                        fixed. Please note that the final exchange rate is only known at the time of the actual
                        transaction and therefore the final payout amount may differ from the amount displayed.Check
                        your bank account details before confirming. Note that once you hit “Confirm” the transfer can
                        no longer be cancelled or reversed.</Text>
                    <TouchableOpacity
                        style={styles.button} onPress={() => {
                        this.payout();
                    }}><Text style={styles.buttonText}>PAY OUT</Text></TouchableOpacity>
                    </View>
                </View>
                }
            </View>);
    }
}

export default PayoutSummaryController;

