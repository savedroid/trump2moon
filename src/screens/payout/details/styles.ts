import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content: {
        justifyContent: "space-between",
        flexGrow: 1,
        alignItems: "center",
        margin: 20
    },
    input : {
        height : 42,
        paddingStart:5,
        marginTop:30,
        color : "rgba(0, 0, 0, 0.64)",
        borderBottomColor : "rgba(0, 0, 0, 0.64)",
        borderLeftColor : "transparent",
        borderTopColor : "transparent",
        borderRightColor : "transparent",
        borderWidth : 1
    },
    inputPaypalAccount : {
        height : 42,
        marginTop: 30,
        paddingStart:5,
        color : "rgba(0, 0, 0, 0.64)",
        borderBottomColor : "rgba(0, 0, 0, 0.64)",
        borderLeftColor : "transparent",
        borderTopColor : "transparent",
        borderRightColor : "transparent",
        borderWidth : 1
    },
    button: {
        backgroundColor: "#00BCFE",
        height: 42,
        width: "100%",
        marginBottom: 5,
        borderRadius: 10,
        paddingTop: 12
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
        fontWeight: "600"
    },
    buttonPayout: {
        borderColor: "#00BCFE",
        height: 42,
        width: "90%",
        marginLeft: "5%",
        marginBottom: 20,
        borderRadius: 10,
        paddingTop: 12
    },
    buttonPayoutText: {
        color: '#00BCFE',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
        fontWeight: "600"
    },
    balance:{
        fontSize:14,
        marginTop:5,
        marginStart:5
    }
})

