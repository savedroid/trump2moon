import React, {Component} from "react";
import {Alert, Text, TextInput, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";
import {Amount} from "../../../model/Amount";

interface Props {
    navigation: any
    route: any
}

interface State {
    amount: Amount,
    paypalEmail: string,
    balance: Amount,
    error: string,
}

class PayoutDetailController extends Component<Props, State> {
    state: State = {
        amount: Amount.zero,
        paypalEmail: "",
        balance: this.props.route.params.balance,
        error: "",
    }
    private amount = React.createRef<TextInput>();
    private bankAccount = React.createRef<TextInput>();
    private reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    private continue() {
        if (this.validate())
            this.props.navigation.push("PayoutSummary", {
                payoutAmount: this.state.amount,
                paypalEmail: this.state.paypalEmail,
            })
    }

    private validate = (): Boolean => {
        if (!this.reg.test(this.state.paypalEmail)) {
            Alert.alert("Enter Valid account number!");

            return false
        }
        if (this.state.amount > this.state.balance) {
            Alert.alert("Enter Payout amount!");
            return false
        }
        return true
    }
    private checkPayoutAmount = (amount: number) => {
        console.log("amount validation", amount, this.state.balance)
        if (this.state.balance.lt(amount)) {
            Alert.alert("Amount can not be bigger than balance!");
            this.setState({amount: Amount.zero});
            this.amount.current?.clear()
        } else
            this.setState({amount: Amount.euroAmount(amount)});
    }

    private validateEmail = () => {
        if (this.state.paypalEmail.length > 0) {
            if (!this.reg.test(this.state.paypalEmail)) {
                Alert.alert("Please enter valid email!");
            } else {
                this.amount.current?.focus()
            }
        } else {
            Alert.alert("Please enter email !");
        }
    }

    render() {

        return (<View style={styles.content}>
            <View style={{flex: 1, width: "100%"}}>

                <TextInput
                    ref={this.bankAccount}
                    style={styles.inputPaypalAccount}
                    placeholder="Your Paypal email address"
                    autoCompleteType={"email"}
                    autoCorrect={false}
                    returnKeyType='next'
                    keyboardType={"email-address"}
                    placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                    onSubmitEditing={() => this.validateEmail()}
                    onChangeText={(value) => this.setState({paypalEmail: value})}
                />

                <TextInput
                    ref={this.amount}
                    style={styles.input}
                    placeholder="Payout amount"
                    keyboardType={"numeric"}
                    autoCorrect={false}
                    placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                    returnKeyType='next'
                    onSubmitEditing={() => this.continue()}
                    onChangeText={(value: String) => this.checkPayoutAmount(+value)}
                />
                <Text style={styles.balance}>Balance {this.state.balance.toString()} €</Text>
            </View>

            <Text style={{marginBottom: 10, paddingStart: 10, fontSize: 14}}>*Note that the money will be transferred to
                your Paypal account, therefore
                double check your Paypal account email address.</Text>
            <TouchableOpacity style={styles.button} onPress={() => {
                this.continue();
            }}><Text style={styles.buttonText}>Continue</Text></TouchableOpacity>
        </View>);
    }
}

export default PayoutDetailController;

