import {Alert, Text, TouchableOpacity, View} from "react-native"
import React, {Component} from "react"
import {styles} from "../savingRule/styles"
import ConfirmationView from "../../components/ConfirmationView"
import {AppSync} from "../../services/appsync";
import {Amount} from "../../model/Amount";
import PaymentService from "../../services/paymentService";

interface Props {
    navigation: any
    route:any
}

interface State {
    payPalSuccess: boolean,
    amount: Amount
}

export default class PayPalController extends Component<Props, State> {

    constructor(props: Props) {
        super(props)
        this.state = {payPalSuccess: false, amount: this.props.route.params.amount}
    }

    private paymentService = new PaymentService(new AppSync());

    private initPayment = async () => {
        this.paymentService.startPayment(this.state.amount).then((result) => {
            this.setState({payPalSuccess: result})
        })
    }

    private onSetupPin() {
        Alert.alert("Set pin")
    }

    render() {
        return (
            <View style={styles.parent}>
                {this.state.payPalSuccess &&
                <View>
                    <ConfirmationView title="Your transaction was successful"
                                      message="Now secure your account by setting up 4 digit PIN." buttonText="SET PIN"
                                      onTouch={this.onSetupPin.bind(this)}/>
                </View>
                }
                {!this.state.payPalSuccess &&
                <View>
                    <Text style={styles.bottomText}>Amount that you are going to select is going to be in EUR.</Text>
                    <TouchableOpacity style={styles.button}
                                      onPress={() => {
                                          this.initPayment()
                                      }
                                      }>
                        <Text style={styles.btnText}>Pay with PayPal</Text>
                    </TouchableOpacity>
                </View>
                }
            </View>
        )
    }
}
