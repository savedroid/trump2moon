import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content: {
        justifyContent: "space-between",
        flexGrow: 1,
        alignItems: "center",
        margin: 20
    },
    image: {
        width: 150,
        height: 150,
        alignSelf: "center",
        marginTop: 20,
        borderRadius: 150 / 2,
        overflow: "hidden",
        borderWidth: 2,
        backgroundColor: "#C4C4C4"
    },
    enjoyYourDay: {
        fontSize: 22,
        marginTop: 30,
        fontWeight: "bold",
        textAlign: "center"
    },
    details: {
        fontSize: 18,
        marginTop: 10,
        marginStart: 40,
        marginEnd: 40,
        textAlign: "center"
    },
    viewNotification: {
        backgroundColor: "#EEECEC",
        height: 100,
        alignSelf: "center",
        margin: 32,
        padding: 10,
        borderRadius: 13,
    },
    button: {
        backgroundColor: "#00BCFE",
        height: 40,
        width: "90%",
        marginBottom: 20,
        borderRadius: 10,
        paddingTop: 12
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,
        fontWeight: "600"
    },
    leftContainer: {
        width: 25,
        height: 25,
        justifyContent: 'flex-start',
    },
    rightContainer: {
        flex: 1,
        textAlign: "right",
        justifyContent: 'flex-end',
    }
});

