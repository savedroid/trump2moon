import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";
import {CognitoService} from "../../services/cognito";

interface Props {
    navigation: any
    route: any
}

class OnBoardingCompletedController extends Component<Props, any> {

    cognito = CognitoService.shared

    onGotIt() {
        this.props.navigation.push("SignIn")
    }

    render() {
        return <View style={styles.content}>
            <View style={{flex: 1}}>
                <Text style={styles.enjoyYourDay}>Enjoy your day </Text>
                <Text style={styles.details}>
                    Once Donald Trump tweets we will notify you so that you can confirm the transaction.</Text>
                <View style={styles.viewNotification}>
                    <View style={{
                        flexDirection: "row",
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                        <Image style={styles.leftContainer} source={require("../../../resources/launch_icon.png")}/>
                        <Text style={{color: "#8A8A8F", fontSize: 13, marginStart: 15}}>TRUMPBIT</Text>
                        <Text style={styles.rightContainer}>now</Text>
                    </View>
                    <Text style={{fontSize: 15, color: "#000000", marginStart: 10, marginEnd: 10, marginTop: 10}}>Hey, Trump
                        has tweeted
                        and now you got 5 € pending.</Text>

                </View>
            </View>


            <TouchableOpacity style={styles.button} onPress={() => {
                this.onGotIt()
            }}><Text style={styles.buttonText}>GOT IT</Text></TouchableOpacity>
        </View>

    }
}

export default OnBoardingCompletedController;

