import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content: {
        justifyContent: "space-between",
        flexGrow: 1,
        alignItems: "center",
        margin: 20
    },
    bottomText: {
        color: "#707070",
        marginBottom: 15
    },
    button: {
        backgroundColor: "#00BCFE",
        height: 40,
        width: "100%",
        borderRadius: 8,
        justifyContent: "center",
    },
    buttonText: {
        color: "#fff",
        textAlign: "center",
        fontSize: 14,
        fontWeight: "600"
    },
    summary: {
        width: "100%",
        height: 80,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center",
    },
    summaryTitle: {
        color: "rgba(0, 0, 0, 0.88)",
        fontSize: 16,
        fontWeight: "600"
    },
    summaryDetail: {
        color: "rgba(0, 0, 0, 0.88)",
        fontSize: 18,
        fontWeight: "600"
    },
});

