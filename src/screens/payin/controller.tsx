import React, {Component} from "react";
import {Text, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";
import {PaymentSummaryText} from "../../components/customViews/PaymentSummaryText";

interface Props {
    navigation: any
    route: any
    amount: string
    rate: string
    fee: string
}

interface State {
    amount: number,
    rate: number,
    fee: number,
}

class PayInController extends Component<Props, State> {
    state = {
        amount: this.props.route.params.amount,
        rate: this.props.route.params.rate,
        fee: this.props.route.params.fee,
    }

    onNext() {
        this.props.navigation.push("PayPal", {amount: this.state.amount})
    }

    render() {
        let {amount, rate, fee} = this.props.route.params
        return (
            <View style={styles.content}>
                <View style={{ flex: 1, alignItems: "center", width: "100%"}}>
                    <PaymentSummaryText title={"Selected amount"} detailText={amount}/>
                    <PaymentSummaryText title={"Exchange rate"} detailText={rate}/>
                    <PaymentSummaryText title={"Transaction fee"} detailText={fee}/>
                    <View style={{width: "120%", height: 1, marginTop: 10, backgroundColor: "rgba(204, 204, 204, 0.8)"}}/>
                    <View style={styles.summary}>
                        <Text style={styles.summaryTitle}>Funding amount</Text>
                        <Text style={styles.summaryDetail}>10,30 €</Text>
                    </View>
                </View>

                <Text style={styles.bottomText}>
                    On the next step we will forward you to PayPal where you will be able to instruct the payment.
                </Text>
                <TouchableOpacity style={styles.button} onPress={() => {
                    this.onNext()
                }}>
                    <Text style={styles.buttonText}>CONTINUE</Text>
                </TouchableOpacity>
            </View>)
    }
}

export default PayInController;

