import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from "react-native";
import { styles } from "./styles";
import { PersonalDetailText } from '../../../components/customViews/PersonalDetailText';

interface Props {
    navigation: any
}

interface State {
    firstname: string
    lastname: string
    birthdate: string
    country: string
    nationality: string
}

class PersonalDetailsController extends Component<Props, State> {
    state = {
        firstname: "First name",
        lastname: "Last name",
        birthdate: "Date of birth",
        country: "Country",
        nationality: "Nationality",
    }

    render() {
        return (
            <View style={styles.personalDetails}>
                <View>
                    <PersonalDetailText text={this.state.firstname} />
                    <PersonalDetailText text={this.state.lastname} />
                    <PersonalDetailText text={this.state.birthdate} />
                    <PersonalDetailText text={this.state.country} />
                    <PersonalDetailText text={this.state.nationality} />
                </View>

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.btnText}>Save</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default PersonalDetailsController;
