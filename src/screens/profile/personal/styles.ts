import {StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    personalDetails: {
        margin: 20,
        justifyContent: "space-between",
        flexGrow: 1
    },
    button: {
        alignItems: "center",
        padding: 20,
        marginTop: 30,
        marginBottom: 20,
        backgroundColor: "#00BCFE",
        borderRadius:20,
    },
    btnText: {
        color: "#fff",
        fontWeight: "bold",
    },
    lineStyle:{
        borderWidth: 0.5,
        borderColor:"black",
        margin:10,
    }
})