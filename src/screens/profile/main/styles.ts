import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content : {
        width : "100%",
        height : "100%",
        flex: 1, 
    },
    header : {
        alignItems: "center"
    },
    list : {
        marginTop: 20
    },
    logo: {
        marginTop: 20,
        width: 64,
        height: 64,
    },
    email: {
        marginTop: 15,
        color: 'rgba(0, 0, 0, 0.64)'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        height : 40,
        marginTop: 20,
        width: '90%',
        backgroundColor: '#00BCFE',
        borderRadius:8,
    },
    btnText: {
        color: '#fff',
        fontWeight: 'bold',
    },
    btnLogout: {
        position: 'absolute',
        padding: 20,
        alignSelf: "flex-end",
    },
    btnLogoutText: {
        color: '#D90080',
        fontWeight: 'bold',
    },
    navigationRightButton: {
        color:'#D90080',
        fontWeight: '600',
        fontSize: 14,
        textTransform: 'uppercase',
        marginRight: 20
    },
})

