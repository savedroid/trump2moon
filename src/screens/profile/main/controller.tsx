import React, { Component } from 'react';
import { styles } from "./styles";
import {Text, Image, TouchableOpacity, View, Alert, Switch} from "react-native";
import { ProfileRow } from "../../../components/customViews/ProfileRow";
import {CognitoService} from "../../../services/cognito";

interface Props {
    navigation: any
}

interface State {
    email: string
}

class ProfileController extends Component<Props, State> {

    constructor(props : any) {
        super(props);
        this.props.navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity onPress={() => Alert.alert("Do you really want to log out?", "", [
                    { text: "Cancel", style: "cancel" },
                    { text: "OK", onPress: () => CognitoService.shared.signOut().then(() => { }), style: "default" } ])}>
                    <Text style={styles.navigationRightButton}>LOG OUT</Text>
                </TouchableOpacity>
            )
        })
    }

    state = {
        email: "email@savedroid.de"
    }

    render() {
        return (<View style={styles.content}>
            <View style={styles.header}>
                <Image style={styles.logo} source={require('../../../../resources/launch_icon.png')} />
                <Text style={styles.email}>{this.state.email}</Text>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.btnText}>VERIFY YOUR ACCCOUNT</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.list}>
                <ProfileRow text={"Log in with Touch ID"} onPressAction={() => Alert.alert('Set Touch ID')} isArrowVisible={false}>
                </ProfileRow>
                <Switch style={{position: 'absolute', right: 20, marginTop: 15}}/>

                <ProfileRow text={"Personal Information"} onPressAction={() => this.props.navigation.push("Details")} isArrowVisible={true}/>
                <ProfileRow text={"Marketing Information"} onPressAction={() => Alert.alert('Marketing Info')} isArrowVisible={true}/>
                <ProfileRow text={"Contact us"} onPressAction={() => Alert.alert('Contact us')} isArrowVisible={true}/>
                <ProfileRow text={"FAQ"} onPressAction={() => Alert.alert('FAQ')} isArrowVisible={true}/>
                <ProfileRow text={""} onPressAction={null}/>
            </View>
        </View>)
    }
}

export default ProfileController;
