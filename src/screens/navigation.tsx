import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import ProfileController from "./profile/main/controller";
import {SettingsController} from "./settings/controller";
import {createStackNavigator} from "@react-navigation/stack";
import SignUpController from "./login/signUp/controller";
import SignInController from "./login/signIn/controller";
import EmailController from "./login/forgotPassword/email/controller";
import ForgotPasswordController from "./login/forgotPassword/controller";
import ConfirmCodeController from "./login/confirmCode/controller";
import OnBoardingCompletedController from "./onboardingComplete/controller";
import React from "react";
import PersonalDetailsController from "./profile/personal/controller";
import SavingRuleController from "./savingRule/controller";
import BalanceController from "./balance/controller";
import PendingBalanceController from './balance/pending/controller';
import PayInController from "./payin/controller";
import PayPalController from "./paypal/controller";
import TermsAndConditionsController from "./login/termsAndConditions/controller";
import ReadmeController from "./login/readme/controller";
import { SessionState } from "../../App";
import {Image, ImageResizeMode} from "react-native";
import PayoutDetailController from "./payout/details/controller";
import PayoutSummaryController from "./payout/summary/controller";

const ProfileStack = createStackNavigator()
const ProfileStackScreen: any = () => {
    return (<ProfileStack.Navigator initialRouteName="Profile">
        <ProfileStack.Screen name="Details" component={PersonalDetailsController} options={{ title: "Personal Details" }} />
        <ProfileStack.Screen name="Profile" component={ProfileController} options={{ title: "Profile" }} />

    </ProfileStack.Navigator>)
}

const BalanceStack = createStackNavigator()
const BalanceStackScreen: any = () => {
    return (<BalanceStack.Navigator initialRouteName="Balance">
        <BalanceStack.Screen name="Balance" component={BalanceController} options={{title : 'Balance'}}/>
        <BalanceStack.Screen name="PendingBalance" component={PendingBalanceController} options={{title: 'Pending amounts'}}/>
        <BalanceStack.Screen name="PayIn" component={PayInController} options={{ title: "Funding Summary" }} />
        <BalanceStack.Screen name="PayPal" component={PayPalController} options={{title: "PayPal"}}/>
        <BalanceStack.Screen name="TermsAndConditions" component={TermsAndConditionsController} options={{title: "TermsAndConditions"}}/>
        <BalanceStack.Screen name="SavingRule" component={SavingRuleController} options={{ title: "Saving Rule" }} />
        <BalanceStack.Screen name="PayoutDetail" component={PayoutDetailController} options={{title : "Payout"}}/>
        <BalanceStack.Screen name="PayoutSummary" component={PayoutSummaryController} options={{title : "PayoutSummary"}}/>

        <BalanceStack.Screen name="Complete" component={OnBoardingCompletedController} options={{ title: "Congrats" }} />
    </BalanceStack.Navigator>)
}

const AppStack = createBottomTabNavigator()
const AppStackScreen: any = () => {
    return (<AppStack.Navigator tabBarOptions={{
        activeTintColor : "#00BCFE",
        inactiveTintColor : "black",
    }}>
        <AppStack.Screen name="Dashboard" component={BalanceStackScreen} options={{tabBarIcon : ({ focused, color, size }) => { return (<Image style={{width: size, height: size, tintColor : focused ? "#00BCFE" : "black"}} source={require("../../resources/dashboard_icon.png")}/>) }}}/>
        <AppStack.Screen name="Profile" component={ProfileStackScreen} options={{tabBarIcon : ({ focused, color, size }) => { return (<Image style={{width: size, height: size, tintColor : focused ? "#00BCFE" : "black"}} source={require("../../resources/profile_icon.png")}/>) }}} />
        <AppStack.Screen name="Settings" component={SettingsController} options={{tabBarIcon : ({ focused, color, size }) => { return (<Image style={{width: size, height: size, tintColor : focused ? "#00BCFE" : "black"}} source={require("../../resources/settings_icon.png")}/>) }}}/>
    </AppStack.Navigator>)
}

const AuthStack = createStackNavigator()
const AuthStackScreen: any = () => {
    return (<AuthStack.Navigator initialRouteName="Login">
        <AuthStack.Screen name="SignIn" component={SignInController} options={{title: "Login"}}/>
        <AuthStack.Screen name="SignUp" component={SignUpController} options={{title: "Registration"}}/>
        <AuthStack.Screen name="VerifyEmail" component={EmailController} options={{title: "Forgot password"}}/>
        <AuthStack.Screen name="ForgotPass" component={ForgotPasswordController} options={{title: "Reset password"}}/>
        <AuthStack.Screen name="Confirm" component={ConfirmCodeController} options={{title: "Verify email address"}}/>
        <AuthStack.Screen name="TermsAndConditions" component={TermsAndConditionsController}
                          options={{title: "Terms and Conditions"}}/>

    </AuthStack.Navigator>);
};

const MigrationStack = createStackNavigator();
const MigrationStackScreen: any = () => {
    return (<MigrationStack.Navigator initialRouteName="Readme">
        <MigrationStack.Screen name="Readme" component={ReadmeController} options={{title: "Read me"}}/>
        <MigrationStack.Screen name="SignUp" component={SignUpController} options={{ title: "Registration" }} />
        <MigrationStack.Screen name="Confirm" component={ConfirmCodeController} options={{ title: "Verify email address" }} />
        <MigrationStack.Screen name="TermsAndConditions" component={TermsAndConditionsController} options={{title: "Terms and Conditions"}}/>
    </MigrationStack.Navigator>);
};

const RootStack = createStackNavigator();
export const RootStackScreen = ({ sessionState }: { [key: string]: SessionState }) => {
    switch (sessionState) {
    case SessionState.LoggedIn:
        return (
            <RootStack.Navigator headerMode="none">
                <RootStack.Screen name="App" component={AppStackScreen} options={{ animationEnabled: true }}></RootStack.Screen>
            </RootStack.Navigator>
        );
    case SessionState.ExistingUser:
        return (
            <RootStack.Navigator headerMode="none">
                <RootStack.Screen name="Migration" component={MigrationStackScreen} options={{ animationEnabled: true }}></RootStack.Screen>
            </RootStack.Navigator>
        );

    default:
        return (
            <RootStack.Navigator headerMode="none">
                <RootStack.Screen name="Auth" component={AuthStackScreen} options={{ animationEnabled: true }}></RootStack.Screen>
            </RootStack.Navigator>
        );
    }
};
