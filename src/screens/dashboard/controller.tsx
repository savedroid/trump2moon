import {Component} from "react";
import * as React from "react";
import {Button, Text, View} from "react-native";
import {AppSync} from "../../services/appsync";



class DashboardController extends Component {

    async add() {
        const as = new AppSync()
        await as.add()
    }

    render(): React.ReactNode {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Button title={"Add"} onPress={this.add}/>
            </View>
        )
    }
}

export default DashboardController
