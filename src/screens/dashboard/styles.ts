import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({

    button : {
        backgroundColor: "#00BCFE",
        height : 40,
        width: "90%",
        marginLeft: "5%",
        marginBottom: 20,
        borderRadius : 10,
        paddingTop:12
    },
    buttonText : {
        color:'#fff',
        textAlign:'center',
        paddingLeft : 10,
        paddingRight : 10,
        fontSize : 16,
        fontWeight : "600"
    }
})

