import {Image, Text, View} from "react-native"
import React, {Component} from "react"
import {styles} from "./styles"


class SplashScreenController extends Component {

    render() {
        return (<View style={styles.container}>
            <Text testID='welcomeText' style={styles.sectionTitle}>TrumpBit and make your first steps in Bitcoin.</Text>
            <Image style={styles.logo} source={require("../../../resources/launch_icon.png")}/>
        </View>)
    }
}

export default SplashScreenController;
