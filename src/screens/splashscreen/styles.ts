import {StyleSheet} from "react-native"
import {Colors} from "react-native/Libraries/NewAppScreen"

export const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
    },
    logo: {
        marginTop: 50,
        width: 200,
        height: 200,
    },
    sectionTitle: {
        fontSize: 16,
        marginStart: 50,
        marginTop: 100,
        marginEnd: 50,
        textAlign: "center",
        fontStyle: "normal",
        display: "flex",
        color: Colors.black,
    }
})

