
import * as React from "react";
import {View, Text, TouchableOpacity} from "react-native";
import {CognitoService} from "../../services/cognito";
import * as queries from "../../graphql/queries"
import * as subscriptions from "../../graphql/subscriptions"
import * as mutations from "../../graphql/mutations"
import {styles} from "../dashboard/styles";
import {AppSync} from "../../services/appsync";
import {NotificationPeriod} from "../../services/API";

const profile = {
    id : "7e04e864-e89b-43bf-b2b9-f630253b34d4",
    amount : {
        amount : 1500,
        currency : "EUR",
        decimals : 2
    },
    notificationPeriod : NotificationPeriod.NONE,
    createdAt : new Date().toISOString()
}
const as = new AppSync()

export const SettingsController = () => {

    const add = async () => {
        let result = await as.do(mutations.deleteProfile, profile.id)
        console.log(result)
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity style={styles.button} onPress={add}><Text style={styles.buttonText}>Logout</Text></TouchableOpacity>

        </View>
    )
}
