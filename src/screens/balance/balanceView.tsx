import {StyleSheet, Text, View} from "react-native";
import React from "react";
import {Amount} from "../../model/Amount";

interface Props {
    balance: Amount
    bitcoinBalance: Amount
}

const BalanceView = (props: Props) => {
    let {balance, bitcoinBalance} = props;
    return (
        <View>
            <Text style={styles.balance}>{balance.toString(true)}</Text>
            <Text style={styles.bitcoinBalance}>({bitcoinBalance.toString()} BTC)</Text>
        </View>);
};

export default BalanceView;

const styles = StyleSheet.create({
        balance: {
            fontSize: 24,
            marginTop: 5,
            textAlign: "center"
        },
        bitcoinBalance: {
            fontSize: 16,
            marginTop: -4,
            textAlign: "center"
        },
    }
);
