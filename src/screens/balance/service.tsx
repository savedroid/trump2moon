import {AppSync} from "../../services/appsync";
import {listExecutions, listProfiles, listTransactions} from "../../graphql/queries";

export default class BalanceService {
    private appSync:AppSync;
    constructor(appSync:AppSync) {
        this.appSync = appSync;
    }
    getDashboardDetails = async () => {
        const result = await Promise.all(
            [this.appSync.do({operation: listTransactions}),
                this.appSync.do({operation: listExecutions}),
                this.appSync.do({operation: listProfiles})
            ]
        );
        console.log("response", result);
        return  result;
    }

}
