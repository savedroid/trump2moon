import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({

    navigationRightButton: {
        color:'#D90080',
        fontWeight: '600',
        fontSize: 14,
        textTransform: 'uppercase',
        marginRight: 20
    },

    container: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        paddingTop: 20,
        paddingBottom: 20

    },
    button: {
        backgroundColor: "#00BCFE",
        height: 42,
        width: "90%",
        marginLeft: "5%",
        marginBottom: 20,
        borderRadius: 10,
        paddingTop: 12
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 16,

        fontWeight: "600"
    }
});

