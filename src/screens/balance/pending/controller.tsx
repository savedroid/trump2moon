import { Component } from "react";
import * as React from "react";
import { View, TouchableOpacity, Text, Alert, Button, Image } from "react-native";
import BalancePendingTableView from '../../../components/tableView/TableView';
import PendingBalanceView from "../../../components/PendingBalanceView"
import { styles } from "./styles";
import {AppSync} from "../../../services/appsync";
import * as mutations from "../../../graphql/customMutations"
import * as queries from "../../../graphql/queries"
import {Amount} from "../../../model/Amount";


interface Props {
    navigation: any
    route: any
}
interface State {
    pendingAmount: Amount
    pendingTransactions:any
}

class PendingBalanceController extends Component<Props, State>{
    state:State={
        pendingAmount:this.props.route.params.pendingAmount,
        pendingTransactions:this.props.route.params.pendingTransactions,
    }
    private onPendingAmountView() {

    }

    private onItemDeleted = async (item) => {
        try {
            const as = new AppSync()
            await as.do({operation : mutations.deleteExecution, id : item.id})
            const executions = await as.do({operation : queries.listExecutions})
            this.setState({pendingTransactions:executions, pendingAmount : Amount.sum(executions.map((e : any) => e.amount))})

        } catch (e) {
            console.log("error", e)
        }
    }
    constructor(props : any) {
        super(props);
        this.props.navigation.setOptions({
            headerRight: () => (
                <CustomNavigationButton
                    title='DELETE ALL'
                    onPress={() => Alert.alert('Delete All Called!')}
                />
            ),
        })
    }
    private convertPendingAmount() {
        this.props.navigation.push("PayIn", {
            amount: this.state.pendingAmount,
            rate: "1 BTC = 8.136.35 € *",
            fee: "3% (0,7 €)",
            owner:this.state.owner
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <PendingBalanceView pendingBalance={this.state.pendingAmount} onTouch={this.onPendingAmountView.bind(this)} />
                <View style={{flex: 1, width:"100%", marginTop:10}}>
                <BalancePendingTableView data = {this.state.pendingTransactions} onItemDelete={(item) => this.onItemDeleted(item)} isCompletedTransaction={false}/>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => {
                    this.convertPendingAmount()
                }}><Text style={styles.buttonText}>Convert pending amount</Text></TouchableOpacity>
            </View>
        )
    }
}
 const CustomNavigationButton = (props : any) => {
    const {textStyle = {}, onPress } = props;
    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={[styles.navigationRightButton, textStyle]}>{props.title}</Text>
        </TouchableOpacity>
    );
};


export default PendingBalanceController;
