import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content: {
        flex:1,
        marginBottom: 10,
        justifyContent:"space-between"
    },
    image: {
        width: 40,
        height: 40,
        marginTop:10,
        alignSelf:"flex-start",
        borderRadius: 150 / 2,
        overflow: "hidden",
        justifyContent: "center",
        backgroundColor: "#C4C4C4"
    },
    list: {
        width: "100%",
        flex : 0.9,
        marginBottom: 5,
    },
    button: {
        backgroundColor: "#00BCFE",
        height: 40,
        borderColor: "#00BCFE",
        alignSelf:"center",
        width: "95%",
        borderRadius: 8,
        borderWidth: 2,
        justifyContent: "center",
    },
    buttonText: {
        color: '#fff',
        textAlign: "center",
        fontSize: 14,
        fontWeight: "bold",
    },
    buttonPayout: {
        borderColor: "#00BCFE",
        height: 40,
        alignSelf:"center",
        width: "95%",        marginTop: 10,
        borderRadius: 8,
        marginBottom:10,
        borderWidth: 1,
        justifyContent: "center",
    },
    buttonPayoutText: {
        color: '#00BCFE',
        textAlign: "center",
        fontSize: 14,
        fontWeight: "bold",
    },
});

