import React, {Component} from "react";
import {FlatList, RefreshControl, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";
import PendingBalanceView from "../../components/PendingBalanceView";
import TransactionListView from "../../components/tableView/TableView";
import BalanceService from "./service";
import LoadingSpinner from "../../components/loadingSpinner";
import CardTweetAmount from "../../components/CardTweetAmountEdit";
import {AppSync} from "../../services/appsync";
import GetStartedScreen from "../savingRule/getStartedScreen";
import BalanceView from "./balanceView";
import {Amount} from "../../model/Amount"
import PendingListItem, {Separator} from "../../components/tableView/TableListItem";


interface Props {
    navigation: any
    route: any
}

enum ViewStates {
    // eslint-disable-next-line no-unused-vars
    LOADING,
    // eslint-disable-next-line no-unused-vars
    BALANCE,
    // eslint-disable-next-line no-unused-vars
    NO_PROFILE
}

interface State {
    pendingAmount: Amount
    balance: Amount
    bitcoinBalance: Amount
    tweetAmount: Amount,
    transactions: any,
    pendingTransactions: any,
    viewState: ViewStates,
    isLoading: boolean,
}

class BalanceController extends Component<Props, State> {
    state: State = {
        pendingAmount: Amount.zero,
        pendingTransactions: {},
        balance: Amount.zero,
        bitcoinBalance: Amount.zero,
        tweetAmount: Amount.zero,
        transactions: {},
        isLoading: false,
        viewState: ViewStates.LOADING
    }

    private getValues = () => {
        this.setState({viewState: ViewStates.LOADING, isLoading: true})
        new BalanceService(new AppSync()).getDashboardDetails().then(([balance, pending, profile]) => {
            console.log(profile);
            this.getBalance(balance.reverse());
            this.getPendingBalance(pending.reverse());
            this.setProfileState(profile)
        }).catch((error) => {
            console.log("Error in dashboard", error);
        });
    };

    private setProfileState = (profile: []) => {
        if (profile.length > 0) {
            this.setState({
                viewState: ViewStates.BALANCE,
                tweetAmount: Amount.parse(profile[0].amount),
                isLoading: false
            });
        } else {
            this.setState({viewState: ViewStates.NO_PROFILE, isLoading: false});

        }
    }
    private getBalance = (transactions: []) => {

        console.log("Balance", transactions);
        let amounts = transactions.map((t: any) => Amount.parse(t.amount).lt(0) ? t.amount : t.convertedAmount)
        let totalBitcoin = Amount.sum(amounts.filter(t => t != null));
        this.setState({
            transactions: transactions,
            balance: Amount.euroAmount(300),
            bitcoinBalance: totalBitcoin
        });
    };

    private getPendingBalance = (pendingTransactions: []) => {
        console.log("pendingTransactions", pendingTransactions);
        let totalBalance = Amount.sum(pendingTransactions.map((t: any) => t.amount));
        this.setState({pendingTransactions: pendingTransactions, pendingAmount: totalBalance});

    };

    componentDidMount(): void {
        this.getValues();

    }


    private convertPendingAmount() {
        this.props.navigation.push("PayIn", {
            amount: this.state.pendingAmount,
            rate: "1 BTC = 8.136.35 € *",
            fee: "3% (0,7 €)",
        });
    }

    private onPendingAmountClick() {
        this.props.navigation.push("PendingBalance", {
            pendingAmount: this.state.pendingAmount,
            pendingTransactions: this.state.pendingTransactions,
        });
    }

    private payout() {
        this.props.navigation.push("PayoutDetail", {balance: this.state.balance});

    }

    private editSavingRule() {
        this.props.navigation.push("SavingRule", {amount: this.state.tweetAmount});
    }

    keyExtractor = (item: any, index: any) => index.toString()

    onPress = (item) => {
    }

    renderItem = ({item}) => (
        <PendingListItem
            item={item}
            onSwipeFromRight={() => {
            }}
            pressOnRight={() =>
                this.onPress(item)
            }
            isCompletedTransaction={true}
        />
    )

    header = () => {
        return (                <View style={{
            backgroundColor : "#efefef",
            marginBottom: 10,
            width: "100%",
        }}><BalanceView balance={this.state.balance}
                             bitcoinBalance={this.state.bitcoinBalance}/>
        {this.state.pendingAmount.gt(0) &&
        <PendingBalanceView pendingBalance={this.state.pendingAmount}
                            onTouch={this.onPendingAmountClick.bind(this)}/>
        }

        <CardTweetAmount tweetAmount={this.state.tweetAmount}
                         editSavingRule={this.editSavingRule.bind(this)}/></View>
    );
    }

    render() {
        console.log(this.state.transactions)
        return (<View style={{
            flexGrow: 1
        }}>
            {this.state.viewState == ViewStates.LOADING && <LoadingSpinner/>}
            {this.state.viewState == ViewStates.NO_PROFILE &&
            <GetStartedScreen onPress={this.editSavingRule.bind(this)}/>}
            {this.state.viewState == ViewStates.BALANCE &&
                <View style={{
                    flexGrow: 1
                }}>
                <FlatList style={styles.list}
                          keyExtractor={this.keyExtractor}
                          data={this.state.transactions}
                          renderItem={this.renderItem}
                          stickyHeaderIndices={[0]}
                          onRefresh={()=> { this.getValues() }}
                          refreshing={this.state.isLoading}
                          ListHeaderComponent={this.header()}
                          ItemSeparatorComponent={() => <Separator/>}
                />
                    {this.state.pendingAmount.gt(0) &&
                    <TouchableOpacity style={styles.button} onPress={() => {
                        this.convertPendingAmount();
                    }}><Text style={styles.buttonText}>CONVERT PENDING AMOUNT</Text></TouchableOpacity>}
                    {this.state.balance.gt(0) &&
                    <TouchableOpacity style={styles.buttonPayout} onPress={() => {
                        this.payout();
                    }}><Text style={styles.buttonPayoutText}>PAY OUT</Text></TouchableOpacity>
                    }
                </View>
            }

        </View>);
    }
}

export default BalanceController;

