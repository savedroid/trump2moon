import {Alert, Text, TouchableOpacity, View} from "react-native";
import React, {Component} from "react";
import {styles} from "./styles";
import CircleSizeSelector from "../../components/customViews/amountPicker/CircleSizeSelector";
import {AppSync} from "../../services/appsync"
import LoadingSpinner from "../../components/loadingSpinner";
import SavingRuleSuccess from "./success_screen";
import {Amount} from "../../model/Amount";
import SavingRuleService from "../../services/service";

interface Props {
    navigation: any
    route: any
}

export enum ViewState {
    // eslint-disable-next-line no-unused-vars
    LOADING,
    // eslint-disable-next-line no-unused-vars
    SUCCESS,
    // eslint-disable-next-line no-unused-vars
    SELECT_AMOUNT
}

interface State {
    amount: Amount
    textSize: number
    viewState: ViewState
}

const InitialValue = Amount.euroAmount(0.5);
const InitialTextSize = 20;

const textSizesToAmounts : {[key:number] : number} = {
    0.5 : InitialTextSize,
    1 : 26,
    2 : 30,
    3 : 34,
    5 : 38,
    10 : 42,
}
class SavingRuleController extends Component<Props, State> {
    private service: SavingRuleService = new SavingRuleService(new AppSync());

    state : State = {
        amount: this.props.route.params.amount,
        textSize: textSizesToAmounts[+this.props.route.params.amount.toString() || +InitialValue.toString()],
        viewState: ViewState.SELECT_AMOUNT
    };




    private getNewTextSize = (selectedAmount: number): number => {
        let textSize: number | undefined = textSizesToAmounts[selectedAmount];
        return textSize !== undefined ? textSize : InitialTextSize;
    };

    private startRuleUpdate = async () => {
        this.setState({viewState: ViewState.LOADING});
        const result = await this.service.saveAmount(this.state.amount);
        if (!result) {
            this.setState({viewState: ViewState.SELECT_AMOUNT});
            Alert.alert("Error", "the amount could not be updated.");
        } else {
            this.setState({viewState: ViewState.SUCCESS});
        }
    };

    onAmountChanged = (value: number) => {
        this.setState({amount: Amount.euroAmount(value), textSize: this.getNewTextSize(value)});
    };

    render() {
        return (
            <View style={styles.parent}>
                {this.state.viewState == ViewState.SUCCESS && <SavingRuleSuccess navigation={this.props.navigation}/>}
                {this.state.viewState == ViewState.LOADING && <LoadingSpinner/>}
                {this.state.viewState == ViewState.SELECT_AMOUNT && <View style={styles.parent}>
                    <View>
                        <Text style={styles.title}>Every time Donald Trump tweets buy Bitcoin for the amount of</Text>
                    </View>
                    <View style={[styles.parent, {height: "60%"}]}>
                        <CircleSizeSelector
                            initialValue={+this.state.amount.toString() || +InitialValue.toString()}
                            manualValues={Object.keys(textSizesToAmounts)}
                            onChange={this.onAmountChanged}>
                            <View>
                                <Text style={{
                                    fontSize: this.state.textSize,
                                    fontWeight: "bold",
                                    color: "#00BCFE",
                                }
                                }>{this.state.amount.toString(true)}</Text>
                            </View>
                        </CircleSizeSelector>
                    </View>

                    <View>
                        <Text style={styles.bottomText}>Amount that you are going to select is going to be in
                            EUR.</Text>
                        <TouchableOpacity style={styles.button} onPress={() => this.startRuleUpdate()}>
                            <Text style={styles.btnText}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                }
            </View>
        );
    }
}

export default SavingRuleController;
