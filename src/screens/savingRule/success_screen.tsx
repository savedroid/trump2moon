import {Component, default as React} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";

interface Props {
    navigation: any
}

export default class SavingRuleSuccess extends Component<Props, any> {
    render(): React.ReactNode {
        return (
            <View style={styles.parent}>
                <View style={{alignItems: "center"}}>
                    <Image style={localStyles.image} source={require('../../../resources/saving_success.png')}/>
                    <Text style={localStyles.title}>Enjoy your day</Text>
                    <Text style={localStyles.message}>Once Donal Trump tweets we will notify you so that you can confirm
                        the transaction.</Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.pop()}><Text
                    style={styles.btnText}>GOT IT</Text></TouchableOpacity>
            </View>);
    }
}
const localStyles = StyleSheet.create({
    image: {
        width: 150,
        height: 150,
        marginTop: 60,
    },
    title: {
        fontSize: 22,
        marginTop: 30,
        textAlign: "center"
    },
    message: {
        fontSize: 16,
        marginTop: 10,
        marginStart: 30,
        marginEnd: 30,
        textAlign: "center"
    },
});