import {Component, default as React} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {styles} from "./styles";
import BalanceView from "../balance/balanceView";

const GetStartedScreen = (props: any) => {
    const onPress = () => {
        props.onPress();
    };
    return (
        <View style={styles.parent}>
            <View style={{alignItems: "center"}}>
                <BalanceView balance={0} bitcoinBalance={0}/>
                <Image style={localStyles.image} source={require("../../../resources/launch_icon.png")}/>
                <Text style={localStyles.message}>Make your first steps in Bitcoin with Donald Trump tweets</Text>
            </View>
            <TouchableOpacity style={styles.button} onPress={onPress}><Text
                style={styles.btnText}>GET STARTED</Text></TouchableOpacity>
        </View>);
};
export default GetStartedScreen;
const localStyles = StyleSheet.create({
    image: {
        width: 150,
        height: 150,
        marginTop: 60,
    },
    title: {
        fontSize: 22,
        marginTop: 30,
        textAlign: "center"
    },
    message: {
        fontSize: 14,
        margin: 30,
        textAlign: "center",
        fontWeight: "bold",
    },
});