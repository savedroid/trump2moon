import {StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    parent: {
        marginStart: 10,
        marginEnd: 10,
        justifyContent: "space-between",
        flexGrow: 1,
        height: "100%"
    },
    button: {
        alignItems: "center",
        marginTop: 20,
        marginBottom: 20,
        paddingTop: 12,
        height: 40,
        backgroundColor: "#00BCFE",
        borderRadius:10,
        width: "100%"
    },
    btnText: {
        color: "#fff",
        fontWeight: "bold",
    },
    title: {
        fontWeight: "bold",
        textAlign: "center"
    },
    bottomText: {
        color: "#707070",
        margin: 10
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    text: {
        fontSize: 28,
        fontWeight: "bold",
        color: "#00BCFE",
    },
});