import React, { Component } from "react";
import { SafeAreaView, Text, TouchableOpacity, View } from "react-native";
import { styles } from "../signUp/styles";
import { Mode } from "../signUp/controller";
import { MigrationService } from "../../../services/migration";

interface Props {
    navigation : any
    route : any
}

interface State {
    email?: string
}

class ReadmeController extends Component<Props, State> {
    text1 = "It’s time to go to moon with our updated app. Now "
    text2 = "savedroid"
    text3 = " app becomes "
    text4 = "Trump2Moon"
    text5 = ". Explore  the improved simlified app. Your balance and crypto currency (if any) will be transferred to the updated app."
    text6 = "In case you do not wish to use our service anymore you can withdraw your balance."

    migrationService = new MigrationService();

    constructor(props : any) {
        super(props);
        this.props.navigation.setOptions({
            header: () => { return null; }
        });

        this.migrationService.checkExisting().then((email: any) => this.setState({email}));
    }

    render() {
        return (<SafeAreaView style={{flex: 1, justifyContent: "space-between"}}>
            <View style={{ marginLeft: "5%", justifyContent: "center", marginTop: 20, marginRight: "5%" }}>
                <Text style={{ fontSize: 16, fontWeight: "bold", alignSelf: "center" }}>App Update</Text>
                <Text style={{ fontSize: 22, fontWeight: "bold", marginTop: 25 }}>Read me first</Text>
                
                <Text style={{ fontSize: 16, marginTop: 25, lineHeight: 25 }}>
                    <Text>{this.text1}</Text>
                    <Text style={{fontWeight: "bold"}}>{this.text2}</Text>
                    <Text>{this.text3}</Text>
                    <Text style={{fontWeight: "bold"}}>{this.text4}</Text>
                    <Text>{this.text5}</Text>
                </Text>
                <Text style={{ fontSize: 16, marginTop: 20, lineHeight: 25 }}>{this.text6}</Text>
            </View>
            <View style={{justifyContent: "center" }}>
                <TouchableOpacity 
                    style={[styles.button, { marginBottom: 12 }]} 
                    onPress={() => this.props.navigation.replace("SignUp", {email: this.state.email, mode: Mode.ExistingUpdate})}>
                    <Text style={styles.buttonText}>CONTINUE WITH THE NEW APP</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={[styles.borderedButton, { marginBottom: 10 }]} 
                    onPress={() => this.props.navigation.push("SignUp", {email: this.state.email, mode: Mode.ExistingTerminate})}>
                    <Text style={styles.borderedButtonText}>PAY OUT AND DELETE MY ACCOUNT</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>);
    }
}

export default ReadmeController;