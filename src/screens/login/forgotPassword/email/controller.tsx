import React, {Component} from "react";
import {Text, TextInput, TouchableOpacity, View, Alert, SafeAreaView} from "react-native";
import {styles} from "../../signUp/styles";
import {CognitoService} from "../../../../services/cognito";

interface Props {
    navigation: any
}

interface State {
    username: string
    error: string
}

class EmailController extends Component<Props, State> {

    state = {
        username: "",
        error: "",
    }
    cognito = CognitoService.shared

    private verifyEmail() {
        if (!this.validate()) {
            return;
        }

        this.cognito.forgotPassword(this.state)
            .then(() => this.props.navigation.push("ForgotPass", this.state))
            .catch(err => {
                this.setState({error: err.message}, this.showError);
            });
    }

    private validate() {
        if (this.state.username === "") {
            this.setState({error: "Please enter an email"}, this.showError);
            return false;
        }
        return true;
    }

    private showError() {
        Alert.alert("An error occured.", this.state.error);
    }

    render() {
        return (<SafeAreaView style={styles.content}>
            <View style={{flex: 1}}>
                <Text
                    style={styles.infoText}>{"We are going to send you a code to your email, enter it on the next screen."}</Text>

                <TextInput
                    style={[styles.input, {marginTop: 50}]}
                    placeholder="Email address"
                    autoCompleteType={"email"}
                    autoCorrect={false}
                    returnKeyType='next'
                    keyboardType={"email-address"}
                    autoCapitalize="none"
                    placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                    onSubmitEditing={() => setTimeout(() => {
                        this.verifyEmail()
                    }, 200)}
                    onChangeText={(value) => this.setState({username: value})}
                />

                <TouchableOpacity style={[styles.button, {marginTop: 12}]} onPress={() => this.verifyEmail()}>
                    <Text style={styles.buttonText}>SEND CODE</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>);
    }
}

export default EmailController;
