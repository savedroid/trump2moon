import React, {Component} from "react";
import {ScrollView, Text, TextInput, TouchableOpacity, View, Alert} from "react-native";
import {styles} from "../signUp/styles";
import {CognitoService} from "../../../services/cognito";
import {loginStyles} from "../signIn/styles";
import PasswordTextBox from "../../../components/customViews/PasswordTextBox";
import {maskEmail} from "../../../helpers/Utils";

interface Props {
    navigation: any
    route: any
}

interface State {
    username: string
    code: string
    password: string
    error: string
}

class ForgotPasswordController extends Component<Props, State> {

    state = {
        username: "",
        code: "",
        password: "",
        error: "",
    }
    cognito = CognitoService.shared
    secondTextInput: any

    constructor(props: any) {
        super(props);
        this.state = {
            username: props.route.params.username,
            code: "",
            password: "",
            error: "",
        }
    }

    private submit() {
        if (!this.validate()) {
            return;
        }

        this.cognito.forgotPasswordSubmit(this.state)
            .then(() => this.cognito.signIn(this.state))
            .then(() => this.cognito.finishSignIn())
            .catch(err => {
                this.setState({error: err.message}, this.showError);
            });
    }

    private validate() {
        if (this.state.code === "") {
            this.setState({error: "Verification code is empty!"}, this.showError);
            return false;
        } else if (this.state.password === "") {
            this.setState({error: "Password can't be empty!"}, this.showError);
            return false;
        }
        return true;
    }

    private resendVerification() {
        this.cognito.forgotPassword(this.state)
            .then(() => Alert.alert("New code has been sent successfully"))
            .catch(err => {
                this.setState({error: err.message}, this.showError);
            });
    }

    private showError() {
        Alert.alert("An error occured.", this.state.error);
    }

    render() {
        return (<View style={styles.content}>
            <ScrollView style={{flex: 1}}>
                <Text style={styles.infoText}>We sent an email to {maskEmail(this.props.route.params.username)}, please
                    enter the
                    code and create a password.</Text>

                <TextInput
                    style={[styles.input, {marginTop: 50}]}
                    placeholder="Verification code"
                    autoCompleteType={"off"}
                    autoCorrect={false}
                    returnKeyType='next'
                    keyboardType={"number-pad"}
                    autoCapitalize="none"
                    placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                    onSubmitEditing={() => this.secondTextInput.focus()}
                    onChangeText={(value) => this.setState({code: value})}
                />

                <PasswordTextBox
                    ref={(input) => {
                        this.secondTextInput = input;
                    }}
                    placeholder="Create password"
                    returnKeyType='send'
                    onSubmitEditing={() => setTimeout(() => {
                        this.submit()
                    }, 200)}
                    onChangeText={(value) => this.setState({password: value})}
                />
                <TouchableOpacity style={[styles.button, {marginTop: 32}]} onPress={() => this.submit()}>
                    <Text style={styles.buttonText}>CONFIRM</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[loginStyles.borderedButton, {marginTop: 14}]}
                                  onPress={() => this.resendVerification()}>
                    <Text style={loginStyles.borderedButtonText}>RESEND VERIFICATION CODE</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>);
    }
}

export default ForgotPasswordController;
