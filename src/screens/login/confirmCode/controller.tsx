import React, { Component } from "react";
import { SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View, Alert } from "react-native";
import { styles } from "../signUp/styles";
import { CognitoService } from "../../../services/cognito";

interface Props {
    navigation : any
    username : string
    password : string
    route : any
}

interface State {
    code : string
    error : string
}

class ConfirmCodeController extends Component<Props, State> {

    state = {
        code : "",
        error : ""
    }
    cognito = CognitoService.shared

    submit() {
        if (this.state.code === "") {
            this.setState({ error : "Please type the verifaction code" });
            return;
        }

        this.cognito.confirmSignUp({username : this.props.route.params.username, code : this.state.code})
            .then(() => this.login())
            .catch(err => this.setState({ error : err.message }));
    }

    resend() {
        this.cognito.resendCode({username : this.props.route.params.username})
            .then(() => Alert.alert("New code has been sent successfully"))
            .catch(err => this.setState({ error : err.message }));
    }

    login() {
        this.cognito.signIn({username: this.props.route.params.username, password: this.props.route.params.password})
            .then(() => this.cognito.finishSignIn())
            .catch(err => this.setState({ error : err.message }));
    }

    render() {
        return (<SafeAreaView style={styles.content}>
            <View style={{flex: 1, justifyContent: "center"}}>
                <Text style={styles.infoText}>We sent an email to {this.props.route.params.username}.</Text>
                <Text style={styles.infoText}>Please copy the code from your email here.</Text>
            </View>

            <View style={{flex: 2, justifyContent: "center"}}>
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder="Verification code"
                        autoCompleteType={"off"}
                        keyboardType={"number-pad"}
                        autoCorrect={false}
                        placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                        returnKeyType='send'
                        onSubmitEditing={() => this.submit()}
                        onChangeText={(value) => this.setState({code: value, error: ""})}
                    />
                    <Text style={{ color: "red", marginLeft: "5%", marginTop: -15}}>{this.state.error}</Text>
                </ScrollView>
            </View>
            <TouchableOpacity style={[styles.button, {marginBottom: 12}]} onPress={() => this.submit()}>
                <Text style={styles.buttonText}>VERIFY</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.borderedButton, { marginBottom: 10 }]} onPress={() => this.resend()}>
                <Text style={styles.borderedButtonText}>RESEND VERIFICATION CODE</Text>
            </TouchableOpacity>
        </SafeAreaView>);
    }
}

export default ConfirmCodeController;
