import React, {Component} from "react";
import {WebView} from "react-native-webview";

class TermsAndConditionsController extends Component<any, any> {
    render() {
        return (
            <WebView source={{uri: "https://www.savedroid.com/app/savedroid_conditions_of_use.pdf"}} startInLoadingState={true}/>);
    }
}

export default TermsAndConditionsController;
