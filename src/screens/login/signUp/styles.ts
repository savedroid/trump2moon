import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    content : {
        justifyContent : "center",
        width : "100%",
        height : "100%",
        backgroundColor : "white"
    },
    input : {
        width: "90%",
        marginLeft: "5%",
        marginBottom: 20,
        height: 42,
        color: "rgba(0, 0, 0, 1)",
        borderBottomColor: "rgba(0, 0, 0, 0.64)",
        borderLeftColor: "transparent",
        borderTopColor: "transparent",
        borderRightColor: "transparent",
        borderWidth: 1
    },
    checkmarkView : {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 5,
    },
    button : {
        backgroundColor: "#00BCFE",
        height : 40,
        width: "90%",
        marginLeft: "5%",
        borderRadius : 8,
        paddingTop: 12
    },
    buttonText : {
        color:"#fff",
        textAlign:"center",
        paddingLeft : 10,
        paddingRight : 10,
        fontSize : 14,
        fontWeight : "600"
    },
    borderedButton: {
        borderColor: "#00BCFE",
        borderWidth: 1,
        height: 40,
        width: "90%",
        marginLeft: "5%",
        borderRadius: 8,
        justifyContent: "center",
    },
    borderedButtonText: {
        color: "#00BCFE",
        textAlign: "center",
        fontSize: 14,
        fontWeight: "600"
    },
    infoText : {
        textAlign: "center",
        fontSize: 16,
        fontWeight: "600",
        color: "rgba(0, 0, 0, 0.88)",
        marginHorizontal: 16,
        marginVertical: 28
    },
    hint : {
        fontSize: 12,
        color: "rgba(0, 0, 0, 0.64)",
        marginLeft: "5%",
        marginTop: 6,
    },
    link : {
        color: "#D90080",
    },
});

