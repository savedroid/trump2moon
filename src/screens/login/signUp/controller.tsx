import React, { Component } from "react";
import { styles } from "./styles";
import { SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View, Alert } from "react-native";
import { CheckBox } from "react-native-elements";
import {CognitoService} from "../../../services/cognito";
import PasswordTextBox from "../../../components/customViews/PasswordTextBox";

export enum Mode {
    New,
    ExistingUpdate,
    ExistingTerminate
}

interface Props {
    navigation : any
    route : any
    
    email: string
    mode: Mode
}

interface State {
    username : string
    password : string
    error : string
    checked: boolean
}

class SignUpController extends Component<Props, State> {
    state = {
        username : "",
        password : "",
        error : "",

        checked: false,
    }
    modeParams = {
        title: "",
        isUsernameLocked: false,
        isTCVisible: true,
        buttonTitle: "REGISTER"
    }
    cognito = CognitoService.shared
    secondTextInput: any

    constructor(props : any) {
        super(props);
        this.state = {
            username : props.route.params.email,
            password: "",
            error: "",
            checked: false
        }; 
    }

    register() {
        if (!this.validate()) {
            return;
        }

        this.cognito.signUp(this.state)
            .then(() => this.props.navigation.push("Confirm", this.state))
            .catch(err => {
                this.setState({ error : err.message}, this.showError);
            });
    }

    validate() {
        if (this.state.username === "" || this.state.password === "") {
            this.setState({ error : "Email or password field is empty"}, this.showError);
            return false;
        } else if (!this.state.checked && this.modeParameters().isTCVisible) {
            this.setState({ error : "Please accept terms and conditions"}, this.showError);
            return false;
        }
        return true;
    }

    modeParameters(): any {
        switch (this.props.route.params.mode) {
        case Mode.New: return {
            title: "Register now and start\n saving in Bitcoin",
            isUsernameLocked: false,
            isTCVisible: true,
            buttonTitle: "REGISTER"};
        case Mode.ExistingUpdate: return {
            title: "Update your account",
            isUsernameLocked: true,
            isTCVisible: true,
            buttonTitle: "REGISTER"};
        case Mode.ExistingTerminate : return {
            title: "Confirm your account\n before paying out",
            isUsernameLocked: true,
            isTCVisible: false,
            buttonTitle: "LOG IN"};
        }
    }

    showError() {
        Alert.alert("An error occured.", this.state.error);
    }

    onOpenTC() {
        this.props.navigation.push("TermsAndConditions");
    }

    render() {
        return (<SafeAreaView style={styles.content}>
            <View style={{flex : 1, justifyContent: "center"}}>
                <Text style={styles.infoText}>{this.modeParameters().title}</Text>
            </View>

            <View style={{flex : 2, justifyContent: "center"}}>
                <ScrollView>
                    <TextInput
                        style={[styles.input, { color: this.modeParameters().isUsernameLocked ? "rgba(0, 0, 0, 0.4)" : "rgba(0, 0, 0, 0.64)" }]}
                        editable={ !this.modeParameters().isUsernameLocked }
                        defaultValue = { this.props.route.params.email }
                        placeholder="Email address"
                        autoCompleteType={"email"}
                        autoCorrect={false}
                        returnKeyType='next'
                        keyboardType={"email-address"}
                        autoCapitalize="none"
                        placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                        onSubmitEditing={() => this.secondTextInput.focus()}
                        onChangeText={(value) => this.setState({username: value})}
                    />

                    <PasswordTextBox
                        ref={(input) => {
                            this.secondTextInput = input;
                        }}
                        placeholder="Password"
                        returnKeyType='done'
                        onChangeText={(value) => this.setState({password: value})}>
                    </PasswordTextBox>

                    <Text style={styles.hint}>Should consist of at least 8 symbols</Text>
                </ScrollView>
            </View>
            {this.modeParameters().isTCVisible &&
                <View style={styles.checkmarkView}>
                    <CheckBox
                        checkedIcon="check-square"
                        uncheckedIcon="square-o"
                        checkedColor="#D90080"
                        checked={this.state.checked}
                        onPress={() => this.setState({ checked: !this.state.checked })} />
                    <Text style={{ marginLeft: -10 }}>I agree to </Text>
                    <TouchableOpacity onPress={() => { this.onOpenTC(); }}>
                        <Text style={styles.link}>Terms and Conditions.</Text>
                    </TouchableOpacity>
                </View>
            }
            
            <TouchableOpacity style={[styles.button, { marginBottom: 20 }]} onPress={() => this.register() }>
                <Text style={styles.buttonText}>{this.modeParameters().buttonTitle}</Text>
            </TouchableOpacity>
        </SafeAreaView>);
    }
}

export default SignUpController;
