import {StyleSheet} from "react-native";

export const loginStyles = StyleSheet.create({
    borderedButton: {
        borderColor: "#00BCFE",
        borderWidth: 1,
        height: 40,
        width: "90%",
        marginLeft: "5%",
        marginBottom: 10,
        borderRadius: 8,
        justifyContent: "center",
    },

    textButton: {
        color: "#00BCFE",
        justifyContent: "center",
        alignSelf: "center",
        textAlign: "center",
        lineHeight: 40,
        width: "90%",
    },

    borderedButtonText: {
        color: "#00BCFE",
        textAlign: "center",
        fontSize: 14,
        fontWeight: "600"
    },

    registerText: {
        color: "rgba(0, 0, 0, 0.64)",
        textAlign: "center",
        fontSize: 16,
        marginBottom: 10,
    },

    logo: {
        alignSelf: "center",
        marginTop: 40,
        width: 66,
        height: 69,
    },
});

