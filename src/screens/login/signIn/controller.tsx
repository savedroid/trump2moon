import React, { Component } from "react";
import { SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View, Image, Alert } from "react-native";
import { styles } from "../signUp/styles";
import { loginStyles } from "../signIn/styles";
import { CognitoService } from "../../../services/cognito";
import {Mode} from "../signUp/controller";
import PasswordTextBox from "../../../components/customViews/PasswordTextBox";

interface Props {
    navigation: any
}

interface State {
    username : string
    password : string
    error : string
}

class SignInController extends Component<Props, State> {

    state = {
        username : "",
        password : "",
        error : "",
    }
    cognito = CognitoService.shared
    secondTextInput: any

    constructor(props: any) {
        super(props);
        this.props.navigation.setOptions({
            header: () => {
                return null;
            }
        });
    }

    login() {
        if (!this.validate()) {
            return;
        }

        this.cognito.signIn(this.state)
            .then(() => this.cognito.finishSignIn())
            .catch(err => {
                this.setState({error: err.message}, this.showError);
            });
    }

    validate() {
        if (this.state.username === "" || this.state.password === "") {
            this.setState({error: "Email or password field is empty"}, this.showError);
            return false;
        }
        return true;
    }

    showError() {
        Alert.alert("An error occured.", this.state.error);
    }

    render() {
        return (<SafeAreaView style={styles.content}>
            <View style={{flex: 1}}>
                <Image style={loginStyles.logo} source={require("../../../../resources/launch_icon.png")} />
            </View>

            <View style={{flex: 2, justifyContent: "center"}}>
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder="Email address"
                        autoCompleteType={"email"}
                        autoCorrect={false}
                        returnKeyType='next'
                        keyboardType={"email-address"}
                        autoCapitalize="none"
                        placeholderTextColor={"rgba(0, 0, 0, 0.64)"}
                        onSubmitEditing={() => this.secondTextInput.focus()}
                        onChangeText={(value) => this.setState({username: value})}
                    />

                    <PasswordTextBox
                        placeholder="Password"
                        returnKeyType='done'
                        onChangeText={(value) => this.setState({password: value})}
                        onSubmitEditing={() => this.login()}>
                    </PasswordTextBox>

                    <TouchableOpacity style={[styles.button, {marginTop: 10}]} onPress={() => this.login()}>
                        <Text style={styles.buttonText}>LOG IN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginTop: 24}} onPress={() => this.props.navigation.push("VerifyEmail")}>
                        <Text style={loginStyles.textButton}>FORGOT PASSWORD</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>

            <Text style={loginStyles.registerText}>Do not have an account yet?</Text>
            <TouchableOpacity style={[loginStyles.borderedButton, { marginBottom: 20 },]} onPress={() => this.props.navigation.push("SignUp", {email: "", mode: Mode.New})}>
                <Text style={loginStyles.borderedButtonText}>REGISTER</Text>
            </TouchableOpacity>
        </SafeAreaView>);
    }
}

export default SignInController;
