const BigInt = require('big.js');

const symbols : {[key:string] : string}  = {
    EUR : "€"
}


export class Amount {


    static zero = new Amount({amount : "0", decimals : 2, currency : "EUR"})
    decimals : number = 2
    amount : string = ""
    currency : string = "EUR"

    constructor(obj : any) {
        let {decimals, amount, currency } = obj
        if(!decimals || !amount || !currency) throw new Error("amount not parsable, input : " + JSON.stringify(obj))
        this.decimals = decimals
        this.amount = amount
        this.currency = currency
    }

    plus(rhs: Amount): Amount {
        if(rhs.currency != this.currency) throw new Error("currencies do not match")
        return new Amount({
            amount: (BigInt(this.amount).plus(BigInt(rhs.amount))).toString(),
            decimals: rhs.decimals,
            currency : rhs.currency
        })
    }

    gt(rhs : number) : boolean {
        return BigInt(this.amount).times(BigInt(10).pow(-this.decimals)).gt(rhs)
    }
    gte(rhs : number) : boolean {
        return BigInt(this.amount).times(BigInt(10).pow(-this.decimals)).gte(rhs)
    }
    lt(rhs : number) : boolean {
        return BigInt(this.amount).times(BigInt(10).pow(-this.decimals)).lt(rhs)
    }
    lte(rhs : number) : boolean {
        return BigInt(this.amount).times(BigInt(10).pow(-this.decimals)).lte(rhs)
    }

    toString(symbol : boolean = false): string {
        return BigInt(this.amount).times(BigInt(10).pow(-this.decimals)).toString() + " " + (symbol ? (symbols[this.currency] ? symbols[this.currency] : this.currency) : "")
    }

    static parse(obj : any) : Amount {
        return new Amount(obj)
    }

    static sum(arr : Array<any>) : Amount {
        if (arr.length == 0) return Amount.zero
        const first = arr.shift()
        return arr.map(a => Amount.parse(a)).reduce((acc, amount) => acc.plus(amount), new Amount(first))
    }

    static euroAmount(amount : number) : Amount {
        return new Amount({decimals : 2, amount : amount * 100, currency : "EUR"})
    }
}



