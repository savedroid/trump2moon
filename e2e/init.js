const detox = require('detox');
const config = require('../package.json').detox;
const adapter = require('detox/runners/jest/adapter');
const specReporter = require('detox/runners/jest/specReporter');

// Set the default timeout
jest.setTimeout(12000);

jasmine.getEnv().addReporter(adapter);

// This takes care of generating status logs on a per-spec basis. By default, jest only reports at file-level.
// This is strictly optional.
jasmine.getEnv().addReporter(specReporter);

beforeEach(async () => {
  if (typeof(device) == 'undefined') {
    await detox.init(config);
  }
  await device.reloadReactNative();
  await adapter.beforeEach();
}, 30000);

afterAll(async () => {
  await adapter.afterAll();
  await detox.cleanup();
});
