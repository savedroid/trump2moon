describe('Registration', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should find welcome text', async () => {
    await element(by.id('testId'));
    await expect(element(by.text('TrumpBit and make your first steps in Bitcoin.')));
  });

});
