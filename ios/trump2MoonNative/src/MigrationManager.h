//
//  MigrationManager.h
//  trump2MoonNative
//
//  Created by Alexander Berkunov on 09.03.20.
//  Copyright © 2020 savedroid AG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MigrationManager : NSObject <RCTBridgeModule>

@property (strong, nonatomic) NSPersistentContainer *persistentContainer;

@end

NS_ASSUME_NONNULL_END
