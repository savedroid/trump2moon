//
//  MigrationManager.m
//  trump2MoonNative
//
//  Created by Alexander Berkunov on 09.03.20.
//  Copyright © 2020 savedroid AG. All rights reserved.
//

#import "MigrationManager.h"
#import <React/RCTLog.h>


@implementation MigrationManager

RCT_EXPORT_MODULE();


RCT_REMAP_METHOD(migrateExistingUser, migrateExistingUserWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    self.persistentContainer = [[NSPersistentContainer alloc] initWithName:@"DataBase"];
    if (!self.persistentContainer) {
      resolve(@[]);
      return;
    }
    
    [self.persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *description, NSError *error) {
      if (error != nil) {
        NSLog(@"Failed to load Core Data stack: %@", error);
        reject(@"no database found", @"no database found", error);
        return;
      }
      
      NSManagedObjectContext *moc = [self.persistentContainer newBackgroundContext];
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"UserMO"];
      
      NSError *fetchError = nil;
      NSArray *results = [moc executeFetchRequest:request error:&fetchError];
      if (!results || results.count == 0) {
        resolve(@[]);
      }
      else {
        NSString *email = [results.firstObject valueForKey:@"email"];
        resolve(@[email]);
      }
    }];
  });
}

@end
