/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var apiTrump2moonGraphQLAPIIdOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIIDOUTPUT
var apiTrump2moonGraphQLAPIEndpointOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIENDPOINTOUTPUT

Amplify Params - DO NOT EDIT */

exports.handler = async (event, context, callback) => {
    var netAmount = {}
    netAmount.amount = -1;
    if (event.feeAmount.amount
      && event.feeAmount.currency === event.amount.currency
      && event.feeAmount.decimals === event.amount.decimals) {
        netAmount.currency = event.amount.currency;
        netAmount.decimals = event.amount.decimals;
        netAmount.amount = event.amount.amount - event.feeAmount.amount;
    }
    callback(null, netAmount);
};
