'use strict';

const app = require('../../index.js');

    test('verifies successful response', async () => {
        const context =  {

        }
        const callback = function(err, result) {
            if (err)
                console.log("ERROR ", err);
            if (result)
                console.log("RESULT ", result);
           return result;
        }

        const event = {
            amount:100,
            feeAmount: {
                amount: 100,
                currency: "EUR"
            }
        }
        const result = await app.handler(event, context, callback)

        expect(result).toBeNull
    });
