const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();

exports.handler = async (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));
  for (const record of event.Records) {
    console.log(record.eventID);
    console.log(record.eventName);
    console.log('DynamoDB Record: %j', record.dynamodb);
    let promises = [];
    const rowUnmarshalled = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);
    const row = JSON.stringify(rowUnmarshalled);
    if (record.eventName === 'INSERT') {
      let params = {
        input: row
      };
      if (rowUnmarshalled.amount.amount > 0) {
        params.stateMachineArn = process.env.STATE_MACHINE_PAY_IN_ARN;
      } else if (rowUnmarshalled.amount.amount < 0) {
        params.stateMachineArn = process.env.STATE_MACHINE_PAY_OUT_ARN;
      } else {
        throw new Error('invalid amount');
      }

      if (record.dynamodb.NewImage.id && record.dynamodb.NewImage.id.S) {
        params.name = record.dynamodb.NewImage.id.S;
      }
      console.log('startExecution');
      await stepfunctions.startExecution(params).promise();
    }
  }
  callback(null, `Successfully processed ${event.Records.length} records.`);
};
