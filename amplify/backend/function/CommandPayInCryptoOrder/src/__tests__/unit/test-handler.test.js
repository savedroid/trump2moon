"use strict"

const app = require("../../index.js")

describe("error tests", () => {

  it("needs owner", async () => {
    const event = {
      "amount": {
        "amount": 100,
        "decimals": 2,
        "currency": "EUR"
      },
      "payment": {
        "provider": "PAYPAL",
        "id": "foobar0053"
      },
      "id": "foobar0053",
      "status": "PENDING",
      "paymentStatus": "DONE",
      "feeAmount": null
    };

    var isError = 0;

    await app.handler(event, {}, function (getError, getSuccess) {
      if (getError) {
        isError = true;
      }
    });

    // should be an error
    expect(isError).toBeTruthy();
  })
})