const requestretry = require('requestretry').defaults({ json: true, maxAttempts: 2 });
const Big     = require('big.js');


let clientSecret   = null;
let accessToken    = null;
let tokenCreatedAt = null;

const getAccessToken = () => {
    return new Promise((resolve, reject) => {
        const fiftyMinutesAgo = new Date();
        fiftyMinutesAgo.setMinutes(fiftyMinutesAgo.getMinutes() - 50);

        if (accessToken && tokenCreatedAt && tokenCreatedAt > fiftyMinutesAgo) {
            return resolve(accessToken);
        }

        const credentials = {
            client: {
                id:     process.env.NEXUS_OAUTH_CLIENT_ID,
                secret: clientSecret,
            },
            auth:   {
                tokenHost: process.env.NEXUS_OAUTH_TOKEN_HOST,
                tokenPath: process.env.NEXUS_OAUTH_TOKEN_PATH
            }
        };

        const oauth2 = require('simple-oauth2').create(credentials);

        const tokenConfig = {
            scope: process.env.NEXUS_OAUTH_CLIENT_SCOPE,
        };

        oauth2.clientCredentials.getToken(tokenConfig)
            .then((result) => {
                accessToken    = oauth2.accessToken.create(result).token.access_token;
                tokenCreatedAt = new Date();
                return resolve(accessToken);
            })
            .catch((error) => {
                console.log('Access Token error', error);
                return reject(error);
            });
    });
};

const getCustomerCode = (userId) => userId.replace(/-/g, '').toUpperCase();

const createCustomer = (userId) => {
    return new Promise((resolve, reject) => {
        getAccessToken()
            .then((accessToken) => {
                const customerCode = getCustomerCode(userId);

                const params = {
                    url:     process.env.NEXUS_API_HOST + '/Customer/create',
                    method:  'POST',
                    headers: {
                        'api_version':   '1.1',
                        'Content-Type':  'application/json',
                        'Authorization': 'Bearer ' + accessToken
                    },
                    json:    {
                        'Email':        'savedroid-customer@savedroid.de',
                        'CustomerCode': customerCode,
                        'Name':         'savedroid user',
                        'IsBusiness':   false,
                        'CountryCode':  'DE',
                        'CurrencyCode': 'EUR',
                        'TrustLevel':   'New',
                        'IP':           '192.0.0.1'
                    }
                };

                requestretry.post('https://proxypass.soa-api.savedroid.com', { body: params }).then((response) => {
                    const body = response.body;

                    if (body) {
                        if (body.CustomerCode && body.CustomerCode.length === 1 && body.CustomerCode[0] === 'Customer already exists.') {
                            return resolve(customerCode);
                        } else {
                            return reject(body);
                        }
                    }

                    return resolve(customerCode);
                }).catch((error) => {
                    return reject(error);
                });
            })
            .catch(reject);
    });
};

const createBuyOrder = (customerCode, cryptoOrder) => {
    return new Promise((resolve, reject) => {
        getAccessToken()
            .then((accessToken) => {

                let paymentMethodCode = process.env.PAYMENT_METHOD;

                if (cryptoOrder.amount.currency !== 'EUR') {
                    throw new Error('unsupported currency');
                }

                const params = {
                    url:     process.env.NEXUS_API_HOST + '/api/Buy/custodian',
                    method:  'POST',
                    headers: {
                        'api_version':   '1.1',
                        'Content-Type':  'application/json',
                        'Authorization': 'Bearer ' + accessToken
                    },
                    json:    {
                        customerCode:      customerCode,
                        currencyCode:      cryptoOrder.amount.currency,
                        cryptoCode:        "BTC",
                        fiatAmount:        parseFloat((cryptoOrder.amount.amount / 100).toFixed(2)),
                        paymentMethodCode: paymentMethodCode,
                        ip:                '192.0.0.1',
                        // make sure we have a unique payment (so one payment can never execute multiple crypto orders
                        paymentInfo:       't2m-' + cryptoOrder.payment.provider + cryptoOrder.payment.id,
                        orderReference:    cryptoOrder.id
                    }
                };

                const logParams = JSON.parse(JSON.stringify(params));
                delete logParams.headers.Authorization;
                console.log('request of create nexus buy order', logParams);

                requestretry.post('https://proxypass.soa-api.savedroid.com', { body: params }).then((response) => {
                    const body = response.body;
                    console.log('response of create nexus buy order', body);

                    if (!body) {
                        return reject({
                            statusCode:    response.statusCode,
                            statusMessage: response.statusMessage
                        });
                    }

                    if (typeof body === 'object' || body.substr(0, 1) === '{') {
                        return reject(body);
                    }

                    return resolve(body);
                }).catch((error) => {
                    return reject(error);
                });
            })
            .catch(reject);
    });
};

module.exports = (oauthClientSecret) => {
    clientSecret = oauthClientSecret;

    return {
        createCustomer:  createCustomer,
        createBuyOrder:  createBuyOrder,
    }
};