module.exports = {
  query: `query getAmount($owner: String!) {
      profileByOwner(owner: $owner) {
        items {
          owner
          amount {amount, currency, decimals}
        }
      }
    }`
}