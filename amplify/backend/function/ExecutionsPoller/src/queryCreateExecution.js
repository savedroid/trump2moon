module.exports = {
  mutation: `mutation createExecution($input: CreateExecutionInput!) {
      createExecution(input: $input) {
        id
        twitterId
        status
        amount {amount, currency, decimals}
        createdAt
        owner
      }
    }`
}