/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var apiTrump2moonGraphQLAPIIdOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIIDOUTPUT
var apiTrump2moonGraphQLAPIEndpointOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIENDPOINTOUTPUT

Amplify Params - DO NOT EDIT */
'use strict';

const AWS = require('aws-sdk');

const SQS = new AWS.SQS({ apiVersion: '2012-11-05' });

const https = require('https');
const appsyncUrl = process.env.API_TRUMP2MOON_GRAPHQLAPIENDPOINTOUTPUT;
const region = process.env.REGION;
const urlParse = require("url").URL;
const endpoint = new urlParse(appsyncUrl).hostname.toString();
const graphqlQueryCreateExecution = require('./queryCreateExecution.js').mutation;
const graphqlQueryAmount = require('./queryAmount.js').query;
const apiKey = process.env.API_KEY;

// Your queue URL stored in the queueUrl environment variable
const QUEUE_URL = process.env.QUEUE_URL;


function poll(callback) {
  const params = {
    QueueUrl: QUEUE_URL,
    MaxNumberOfMessages: 10, // receiveMessage does support max. 10 items
  };
  // batch request messages
  SQS.receiveMessage(params, (err, data) => {
    if (err) {
      return callback(err);
    }

    if (data.Messages) {
      let promises = [];
      const getAmount = function (owner) {
        return new Promise(function (resolve) {
          const req = new AWS.HttpRequest(appsyncUrl, region);

          const item = {
            owner: owner
          };

          req.method = "POST";
          req.headers.host = endpoint;
          req.headers["Content-Type"] = "application/json";
          req.body = JSON.stringify({
            query: graphqlQueryAmount,
            operationName: "getAmount",
            variables: item
          });

          if (apiKey) {
            req.headers["x-api-key"] = apiKey;
          } else {
            const signer = new AWS.Signers.V4(req, "appsync", true);
            signer.addAuthorization(AWS.config.credentials, AWS.util.date.getDate());
          }

          const data = new Promise((innerresolve) => {
            console.log('starting httprequest');
            const httpRequest = https.request({ ...req, host: endpoint }, (result) => {
              result.on('data', (data) => {
                const string = data.toString();
                console.log('string', string)
                const jsonString = JSON.parse(string)
                if (typeof jsonString.errors !== 'undefined') {
                  console.log('string.errors', jsonString.errors);
                  throw new Error(JSON.stringify(jsonString.errors));
                }
                // set default
                let amount = {
                  'amount': 100,
                  'currency': 'EUR',
                  'decimals': 2
                };
                if (jsonString.data.profileByOwner.items.length !== 0) {
                  amount = jsonString.data.profileByOwner.items[0].amount;
                }
                innerresolve(amount);
              }).on('error', (error) => {
                console.log('error', error);
                throw error;
              });
            });

            httpRequest.write(req.body);
            console.log('req.body', req.body);
            httpRequest.end();
            console.log('end request');
          });
        });
      };

      const createExecution = function (message) {
        const messageDetails = JSON.parse(message.Body);
        return new Promise(function(resolve) {
          getAmount(messageDetails.Username).then(function(amount) {
            const req = new AWS.HttpRequest(appsyncUrl, region);

            const numberPattern = /\d+/g;
            const twitterId = messageDetails.twitterId.match( numberPattern ).join('');

            const item = {
              input: {
                'id': messageDetails.Username + '-' + twitterId,
                'twitterId': messageDetails.twitterId,
                'status': 'PENDING',
                'amount': amount,
                'createdAt': new Date().toISOString(),
                'owner': messageDetails.Username
              }
            };

            req.method = "POST";
            req.headers.host = endpoint;
            req.headers["Content-Type"] = "application/json";
            req.body = JSON.stringify({
              query: graphqlQueryCreateExecution,
              operationName: "createExecution",
              variables: item
            });

            if (apiKey) {
              req.headers["x-api-key"] = apiKey;
            } else {
              const signer = new AWS.Signers.V4(req, "appsync", true);
              signer.addAuthorization(AWS.config.credentials, AWS.util.date.getDate());
            }

            const data = new Promise((innerresolve) => {
              console.log('starting httprequest');
              const httpRequest = https.request({ ...req, host: endpoint }, (result) => {
                result.on('data', (data) => {
                  const string = data.toString();
                  console.log('string', string)
                  const jsonString = JSON.parse(string)
                  if (typeof jsonString.errors !== 'undefined') {
                    console.log('string.errors', jsonString.errors);
                    throw new Error(JSON.stringify(jsonString.errors));
                  }
                  innerresolve(jsonString);
                }).on('error', (error) => {
                  console.log('error', error);
                  throw error;
                });
              });

              httpRequest.write(req.body);
              console.log('req.body', req.body);
              httpRequest.end();
              console.log('end request');
            });

            data.then(function () {
              // delete message
              const params = {
                QueueUrl: QUEUE_URL,
                ReceiptHandle: message.ReceiptHandle,
              };
              SQS.deleteMessage(params,
                function(err, data) {
                  if (err) {
                    console.log(err, err.stack); // an error occurred
                    throw err;
                  }
                  else {
                    console.log(data);           // successful response
                    return resolve();
                  }
                });
            });
          });
        })
      };
      for (let i = 0; i < data.Messages.length; i++) {
        const message = data.Messages[i];
        promises.push(createExecution(message));
      }
      Promise.all(promises).then(function() {
        const result = `Messages received: ${data.Messages.length}`;
        console.log(result);
        callback(null, result);
      });
    } else {
      const result = `No new messages`;
      console.log(result);
      callback(null, result);
    }
  });
}

exports.handler = (event, context, callback) => {
  try {
    // invoked by schedule
    poll(callback);
  } catch (err) {
    callback(err);
  }
};