module.exports = {
  query: `query getKycLevel($owner: String!) {
      profileByOwner(owner: $owner) {
        items {
          owner
          kycLevel
        }
      }
    }`
}