/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var apiTrump2moonGraphQLAPIIdOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIIDOUTPUT
var apiTrump2moonGraphQLAPIEndpointOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIENDPOINTOUTPUT

Amplify Params - DO NOT EDIT */

const AWS = require('aws-sdk');

const https = require('https');
const appsyncUrl = process.env.API_TRUMP2MOON_GRAPHQLAPIENDPOINTOUTPUT;
const region = process.env.REGION;
const urlParse = require("url").URL;
const endpoint = new urlParse(appsyncUrl).hostname.toString();
const graphqlQueryKycLevel = require('./queryKycLevel.js').query;
const apiKey = process.env.API_KEY;

exports.handler = async (event, context, callback) => {

  const kycLevel = new Promise((resolve) => {
    const item = {
      'owner': event.owner
    };

    const req = new AWS.HttpRequest(appsyncUrl, region);

    req.method = "POST";
    req.headers.host = endpoint;
    req.headers["Content-Type"] = "application/json";
    req.body = JSON.stringify({
      query: graphqlQueryKycLevel,
      operationName: "getKycLevel",
      variables: item
    });

    if (apiKey) {
      req.headers["x-api-key"] = apiKey;
    } else {
      const signer = new AWS.Signers.V4(req, "appsync", true);
      signer.addAuthorization(AWS.config.credentials, AWS.util.date.getDate());
    }

    console.log('starting httprequest');
    const httpRequest = https.request({ ...req, host: endpoint }, (result) => {
      result.on('data', (data) => {
        var string = data.toString();
        console.log('string', string)
        var jsonString = JSON.parse(string)
        if (typeof jsonString.errors !== 'undefined') {
          console.log('string.errors', jsonString.errors);
          throw new Error(JSON.stringify(jsonString.errors));
        }
        let kycLevel;
        if (jsonString.data.profileByOwner.items.length === 0) {
          kycLevel = 0;
        } else {
          kycLevel = jsonString.data.profileByOwner.items[0].kycLevel
        }
        resolve(kycLevel);
      }).on('error', (error) => {
        console.log('error', error);
        throw error;
      });
    });

    httpRequest.on('error', (error) => {
      console.log('error', error);
      throw error;
    });

    httpRequest.write(req.body);
    console.log('req.body', req.body);
    httpRequest.end();
    console.log('end request');
  });

  await kycLevel.then(function (kycLevel) {
    console.log('kyc level', kycLevel);
    if (kycLevel !== 2) {
      throw new Error('invalid kyc');
    }
    callback(null, {});
  }).catch(function (error) {
    console.log('error', error);
    throw error;
  });
};
