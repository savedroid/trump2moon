module.exports = {
  mutation: `mutation updateTransaction($input: UpdateTransactionInput!) {
      updateTransaction(input: $input) {
        id
        feeAmount {amount, currency, decimals}
      }
    }`
}