module.exports = {
  query: `query transactionByOwner($owner: String!) {
      transactionByOwner(owner: $owner) {
        items {
          id
          amount {amount, currency, decimals}
          status
          owner
        }
      }
    }`
}