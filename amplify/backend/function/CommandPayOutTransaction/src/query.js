module.exports = {
  mutation: `mutation updateTransaction($input: UpdateTransactionInput!) {
      updateTransaction(input: $input) {
        id
        convertedAmount {amount, currency, decimals}
        status
        nexusTransaction
        payout
      }
    }`
}