/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var storageNexusCustomerName = process.env.STORAGE_NEXUSCUSTOMER_NAME
var storageNexusCustomerArn = process.env.STORAGE_NEXUSCUSTOMER_ARN

Amplify Params - DO NOT EDIT */


const AWS            = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient({region: 'eu-central-1'});
const Promise        = require('bluebird');
const storageNexusCustomerName = process.env.STORAGE_NEXUSCUSTOMER_NAME;

const nexusCustomers   = {};
const getNexusCustomer = (userId, nexusClient) => {
  return new Promise((resolve, reject) => {
    if (typeof nexusCustomers[userId] !== 'undefined') {
      return resolve(nexusCustomers[userId]);
    }

    const params = {
      TableName: storageNexusCustomerName,
      Key:       {owner: userId}
    };

    documentClient.get(params, (error, result) => {
      if (error) {
        return reject(error);
      }

      if (result && result.Item) {
        nexusCustomers[userId] = result.Item;

        return resolve(nexusCustomers[userId]);
      } else {
        console.log('params', params);
        throw new Error('nexus customer does not exist');
      }
    });
  });
};

const processOrder = (cryptoOrder, nexusClient) => {
  return new Promise((resolve) => {
    if (!cryptoOrder.owner) {
      console.log('crypto order without owner', cryptoOrder);
      throw new Error('crypto order without owner');
    }
    getNexusCustomer(cryptoOrder.owner, nexusClient)
      .then((nexusCustomer) => {
        if (!nexusCustomer) {
          console.log('nexus customer not found for owner ' + cryptoOrder.owner);
          throw new Error('nexus customer not found for owner ' + cryptoOrder.owner);
        }
        const nexusClientOrderAction = nexusClient.createSellOrder.bind(nexusClient);

        nexusClientOrderAction(nexusCustomer.nexusCustomerCode, cryptoOrder)
          .then((transactionPath) => {
            console.log('created nexus transaction ' + transactionPath + ' for order ', cryptoOrder);
            return resolve(transactionPath);
          })
          .catch((error) => {
            console.log('error on creating order', JSON.stringify(cryptoOrder), JSON.stringify(error));
            throw new Error('error on creating order');
          });
      });
  });
};

exports.handler = (event, context, callback) => {
  const handleError = (error) => {
    console.log('handleError', error);
    callback(error);
  };

  const handleSuccess = (returnValue) => {
    callback(null, returnValue);
  };

  const oauthClientSecret = process.env.NEXUS_OAUTH_CLIENT_SECRET;

  const nexusClient = require('./nexus.js')(oauthClientSecret);

  console.log('processing order', event);
  processOrder(event, nexusClient)
    .then(handleSuccess)
    .catch(handleError);
};