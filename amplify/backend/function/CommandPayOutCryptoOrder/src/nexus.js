const requestretry = require('requestretry').defaults({ json: true, maxAttempts: 2 });
const Big     = require('big.js');


let clientSecret   = null;
let accessToken    = null;
let tokenCreatedAt = null;

const getAccessToken = () => {
    return new Promise((resolve, reject) => {
        const fiftyMinutesAgo = new Date();
        fiftyMinutesAgo.setMinutes(fiftyMinutesAgo.getMinutes() - 50);

        if (accessToken && tokenCreatedAt && tokenCreatedAt > fiftyMinutesAgo) {
            return resolve(accessToken);
        }

        const credentials = {
            client: {
                id:     process.env.NEXUS_OAUTH_CLIENT_ID,
                secret: clientSecret,
            },
            auth:   {
                tokenHost: process.env.NEXUS_OAUTH_TOKEN_HOST,
                tokenPath: process.env.NEXUS_OAUTH_TOKEN_PATH
            }
        };

        const oauth2 = require('simple-oauth2').create(credentials);

        const tokenConfig = {
            scope: process.env.NEXUS_OAUTH_CLIENT_SCOPE,
        };

        oauth2.clientCredentials.getToken(tokenConfig)
            .then((result) => {
                accessToken    = oauth2.accessToken.create(result).token.access_token;
                tokenCreatedAt = new Date();
                return resolve(accessToken);
            })
            .catch((error) => {
                console.log('Access Token error', error);
                return reject(error);
            });
    });
};

const getCustomerCode = (userId) => userId.replace(/-/g, '').toUpperCase();

const createSellOrder = (customerCode, cryptoOrder) => {
    return new Promise((resolve, reject) => {
        getAccessToken()
            .then((accessToken) => {

                let paymentMethodCode;
                switch (cryptoOrder.amount.currency) {
                  case 'BTC':
                      paymentMethodCode = process.env.PAYMENT_METHOD_BTC;
                      break;
                  case 'ETH':
                    paymentMethodCode = process.env.PAYMENT_METHOD_ETH;
                    break;
                  case 'BCH':
                    paymentMethodCode = process.env.PAYMENT_METHOD_BCH;
                    break;
                  case 'LTC':
                    paymentMethodCode = process.env.PAYMENT_METHOD_LTC;
                    break;
                  default:
                      console.log('cryptoOrder.amount', cryptoOrder.amount);
                      throw new Error('unsupported currency');
                }

                const params = {
                    url:     process.env.NEXUS_API_HOST + '/sell/custodian',
                    method:  'POST',
                    headers: {
                        'api_version':   '1.1',
                        'Content-Type':  'application/json',
                        'Authorization': 'Bearer ' + accessToken
                    },
                    json:    {
                        customerCode:      customerCode,
                        currencyCode:      'EUR',
                        cryptoCode:        cryptoOrder.amount.currency,
                        cryptoAmount:      Big(cryptoOrder.amount.amount * -1).div(Big(10).pow(cryptoOrder.amount.decimals)).toFixed(8),
                        paymentMethodCode: paymentMethodCode,
                        ip:                '192.0.0.1',
                        paymentInfo:       't2m-' + cryptoOrder.id,
                        orderReference:    cryptoOrder.id
                    }
                };

                const logParams = JSON.parse(JSON.stringify(params));
                delete logParams.headers.Authorization;
                console.log('request of create nexus sell order', logParams);

                requestretry.post('https://proxypass.soa-api.savedroid.com', { body: params }).then((response) => {
                    const body = response.body;
                    console.log('response of create nexus sell order', body);

                    if (!body) {
                        return reject({
                            statusCode:    response.statusCode,
                            statusMessage: response.statusMessage
                        });
                    }

                    if (typeof body === 'object' || body.substr(0, 1) === '{') {
                        return reject(body);
                    }

                    return resolve(body);
                }).catch((error) => {
                    return reject(error);
                });
            })
            .catch(reject);
    });
};

module.exports = (oauthClientSecret) => {
    clientSecret = oauthClientSecret;

    return {
        createSellOrder:  createSellOrder,
    }
};