/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var authTrump2moon7e4af070UserPoolId = process.env.AUTH_TRUMP2MOON7E4AF070_USERPOOLID

Amplify Params - DO NOT EDIT */

'use strict';

const AWS = require('aws-sdk');

const SQS = new AWS.SQS({apiVersion: '2012-11-05'});
const Lambda = new AWS.Lambda({apiVersion: '2015-03-31'});

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

// Your queue URL stored in the queueUrl environment variable
const QUEUE_URL = process.env.QUEUE_URL;
const TARGET_QUEUE_URL = process.env.TARGET_QUEUE_URL;
const PROCESS_MESSAGE = 'process-message';
const USER_POOL_ID = process.env.AUTH_TRUMP2MOON7E4AF070_USERPOOLID;


function invokePoller(functionName, message) {
    const payload = {
        operation: PROCESS_MESSAGE,
        message,
    };
    const params = {
        FunctionName: functionName,
        InvocationType: 'Event',
        Payload: new Buffer(JSON.stringify(payload)),
    };
    return new Promise((resolve, reject) => {
        Lambda.invoke(params, (err) => (err ? reject(err) : resolve()));
    });
}


function processMessage(message, callback) {
    console.log(message);

    var messageDetails = JSON.parse(message.Body);

    var deleteMessage = function () {
        // delete message
        const params = {
            QueueUrl: QUEUE_URL,
            ReceiptHandle: message.ReceiptHandle,
        };
        SQS.deleteMessage(params, (err) => callback(err, message));
    }

    if (messageDetails.twitterId) {
        var sendExecutions = function (token) {
            return new Promise(
                function (resolve) {
                    var params = {
                        UserPoolId: USER_POOL_ID,
                        AttributesToGet: [
                            'sub'
                        ],
                        PaginationToken: token
                    };
                    cognitoidentityserviceprovider.listUsers(params, function (err, data) {
                        if (err) {
                            console.log(err, err.stack); // an error occurred
                            throw err;
                        } else {
                            console.log(data);           // successful response
                            var messages = []
                            for (var i = 0; i < data.Users.length; i++) {
                                messages.push(
                                    new Promise(function (resolve) {
                                        var params = {
                                            MessageBody: JSON.stringify(
                                                {
                                                    'Username': data.Users[i].Username,
                                                    'twitterId': messageDetails.twitterId
                                                }),
                                            QueueUrl: TARGET_QUEUE_URL,
                                        };
                                        SQS.sendMessage(params, function (err, data) {
                                            if (err) {
                                                console.log(err, err.stack); // an error occurred
                                                throw err;
                                            } else {
                                                console.log(data);           // successful response
                                                return resolve();
                                            }
                                        });
                                    })
                                );
                            }
                            Promise.all(messages).then(
                                function () {
                                    if (data.PaginationToken) {
                                        return sendExecutions(data.PaginationToken);
                                    } else {
                                        return resolve();
                                    }
                                }
                            );
                        }
                    });
                }
            );
        }

        sendExecutions(null).then(deleteMessage);
    } else {
        console.log('invalid message body ' + message.Body);
        deleteMessage();
    }

}

function poll(functionName, callback) {
    const params = {
        QueueUrl: QUEUE_URL,
        MaxNumberOfMessages: 1,
    };
    // batch request messages
    SQS.receiveMessage(params, (err, data) => {
        if (err) {
            return callback(err);
        }
        if (data.Messages) {
            // for each message, reinvoke the function
            console.log(data);
            const promises = data.Messages.map((message) => invokePoller(functionName, message));
            // complete when all invocations have been made
            Promise.all(promises).then(() => {
                const result = `Messages received: ${data.Messages.length}`;
                console.log(result);
                callback(null, result);
            });
        } else {
            const result = `No new messages`;
            console.log(result);
            callback(null, result);
        }
    });
}

exports.handler = (event, context, callback) => {
    try {
        if (event.operation === PROCESS_MESSAGE) {
            // invoked by poller
            processMessage(event.message, callback);
        } else {
            // invoked by schedule
            poll(context.functionName, callback);
        }
    } catch (err) {
        callback(err);
    }
};
