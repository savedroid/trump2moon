/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION

Amplify Params - DO NOT EDIT */


const AWS            = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient({region: 'eu-central-1'});
const Promise        = require('bluebird');

const getNexusTransaction = (transactionPath, nexusClient) => {
  return new Promise((resolve, reject) => {
    nexusClient.getTransaction(transactionPath)
      .then((nexusTransaction) => {
        console.log('found transaction for transaction code ' + transactionPath, nexusTransaction);
        return resolve(nexusTransaction);
      })
      .catch((error) => {
        console.log('could not find nexus transaction with code ' + transactionPath, error);
        return reject();
        throw new Error('could not find nexus transaction with code ' + transactionPath)
      });
  });
};

exports.handler = (event, context, callback) => {
  const handleError = (error) => {
    console.log('handleError', error);
    callback(error);
  };

  const handleSuccess = (returnValue) => {
    callback(null, returnValue);
  };

  const oauthClientSecret = process.env.NEXUS_OAUTH_CLIENT_SECRET;

  const nexusClient = require('./nexus.js')(oauthClientSecret);

  console.log('processing order', event);
  getNexusTransaction(event.cryptoOrderTransactionPath, nexusClient)
    .then(function (nexusTransaction) {
        var status;
        if (nexusTransaction.status === 'SELLCOMPLETED') {
            status = 'DONE';
            handleSuccess({
              'status': status,
              'convertedAmount': {
                'amount': Math.round(nexusTransaction.payout * 100),
                'currency': 'EUR',
                'decimals': 2
              },
              'nexusTransaction': nexusTransaction
            })
        } else {
            status = 'PENDING';
            handleSuccess({'status': status, 'nexusTransaction': nexusTransaction})
        }
    },
    function () {
        handleSuccess({'status': 'NOTFOUND'})
    })
    .catch(handleError);
};