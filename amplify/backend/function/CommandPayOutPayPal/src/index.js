var paypal = require('paypal-rest-sdk');
paypal.configure({
  'mode': process.env.PAYPAL_ENVIRONMENT, //sandbox or live
  'client_id': process.env.PAYPAL_CLIENT_ID,
  'client_secret': process.env.PAYPAL_CLIENT_SECRET
});

exports.handler = async (event, context, callback) => {
    console.log(event);
    if (event.payment.provider === "PAYPAL") {
      const getPayoutResponse = new Promise((resolve, reject) => {
        const sender_batch_id = event.id;

        if (event.netAmount.currency !== 'EUR') {
          throw new Error('unsupported currency');
        }

        const create_payout_json = {
          "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "recipient_type": "EMAIL",
            "email_subject": "Trump payout"
          },
          "items": [
            {
              "amount": {
                "value": parseFloat((event.netAmount.amount / 100).toFixed(2)),
                "currency": event.netAmount.currency
              },
              "receiver": event.payment.id
            }
          ]
        };

        const sync_mode = 'true';

        paypal.payout.create(create_payout_json, function (error, payout) {
          if (error) {
            console.log(error);
            throw error;
          } else {
            console.log("Create Payout Response");
            console.log(JSON.stringify(payout));
            callback(null, payout)
          }

        });
      });
      await getPayoutResponse.then(function(payout) {
        callback(null, payout);
      })

    } else {
      throw new Error('unsupported payout method');

    }
};


