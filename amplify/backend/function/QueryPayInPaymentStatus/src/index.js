const paypal = require('paypal-rest-sdk');
paypal.configure({
  'mode': process.env.PAYPAL_ENVIRONMENT, //sandbox or live
  'client_id': process.env.PAYPAL_CLIENT_ID,
  'client_secret': process.env.PAYPAL_CLIENT_SECRET
});

exports.handler = async (event, context, callback) => {
    console.log(event);
    if (event.payment.provider === "PAYPAL") {

      const paymentId = event.payment.id;

      const getStatus = new Promise(function (resolve) {
        paypal.payment.get(paymentId, function (error, payment) {
          if (error) {
            console.log(error);
            throw error;
          } else {
            console.log("Get Payment Response");
            console.log(JSON.stringify(payment));
            let status = 'PENDING';
            if (payment.state === 'approved' && payment.intent === 'sale') {
              const transaction = payment.transactions[0];
              if (transaction.amount.currency === event.amount.currency && event.amount.decimals === 2 &&
                parseInt(transaction.amount.total * 100) === event.amount.amount) {
                status = 'DONE'
              }
            } else if (payment.state === 'failed') {
              status = 'FAILED';
            }
            resolve(status)
          }

        });
      });

      await getStatus.then(function (status) {callback(null, status)});
    } else {
      const paymentStatus = 'FAILED';
      callback(null, paymentStatus);

    }
};


