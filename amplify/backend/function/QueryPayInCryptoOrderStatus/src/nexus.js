const requestretry = require('requestretry').defaults({ json: true, maxAttempts: 2 });
const Big     = require('big.js');

let clientSecret   = null;
let accessToken    = null;
let tokenCreatedAt = null;

const getAccessToken = () => {
    return new Promise((resolve, reject) => {
        const fiftyMinutesAgo = new Date();
        fiftyMinutesAgo.setMinutes(fiftyMinutesAgo.getMinutes() - 50);

        if (accessToken && tokenCreatedAt && tokenCreatedAt > fiftyMinutesAgo) {
            return resolve(accessToken);
        }

        const credentials = {
            client: {
                id:     process.env.NEXUS_OAUTH_CLIENT_ID,
                secret: clientSecret,
            },
            auth:   {
                tokenHost: process.env.NEXUS_OAUTH_TOKEN_HOST,
                tokenPath: process.env.NEXUS_OAUTH_TOKEN_PATH
            }
        };

        const oauth2 = require('simple-oauth2').create(credentials);

        const tokenConfig = {
            scope: process.env.NEXUS_OAUTH_CLIENT_SCOPE,
        };

        oauth2.clientCredentials.getToken(tokenConfig)
            .then((result) => {
                accessToken    = oauth2.accessToken.create(result).token.access_token;
                tokenCreatedAt = new Date();
                return resolve(accessToken);
            })
            .catch((error) => {
                console.log('Access Token error', error);
                return reject(error);
            });
    });
};

const getTransaction = (transactionPath) => {
  return new Promise((resolve, reject) => {
    getAccessToken()
      .then((accessToken) => {
        const params = {
          url:     process.env.NEXUS_API_HOST + transactionPath,
          method:  'GET',
          headers: {
            'api_version':   '1.1',
            'Content-Type':  'application/json',
            'Authorization': 'Bearer ' + accessToken
          }
        };

        requestretry.post('https://proxypass.soa-api.savedroid.com?depth=1', { body: params }).then((response) => {
          const body = JSON.stringify(response.body);
          console.log('nexus transaction response body', body, transactionPath);

          try {
            const bodyJson = JSON.parse(body);

            return resolve(bodyJson);
          } catch (e) {
            console.log('#cloudalam error parsing response body', e, body, response);

            return reject(e);
          }
        }).catch((error) => {
          return reject(error);
        });
      })
      .catch(reject);
  });
};

module.exports = (oauthClientSecret) => {
    clientSecret = oauthClientSecret;

    return {
        getTransaction:  getTransaction
    }
};