/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var apiTrump2moonGraphQLAPIIdOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIIDOUTPUT
var apiTrump2moonGraphQLAPIEndpointOutput = process.env.API_TRUMP2MOON_GRAPHQLAPIENDPOINTOUTPUT

Amplify Params - DO NOT EDIT */

exports.handler = async (event, context, callback) => {
    console.log('event', event);
    console.log('event.cryptoOrder', event.cryptoOrder);
    let netAmount = {};
    netAmount.amount = -1;
    if (event.feeAmount.amount
      && event.feeAmount.currency === event.cryptoOrder.convertedAmount.currency
      && event.feeAmount.decimals === event.cryptoOrder.convertedAmount.decimals) {
        netAmount.currency = event.cryptoOrder.convertedAmount.currency;
        netAmount.decimals = event.cryptoOrder.convertedAmount.decimals;
        netAmount.amount = event.cryptoOrder.convertedAmount.amount - event.feeAmount.amount;
    } else {
        throw new Error('unsupported currency');
    }
    callback(null, netAmount);
};
