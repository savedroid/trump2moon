// this is just for documentation purposes, real code is in PayOut-cloudformation-template.json, please keep in sync
{
  "StartAt": "QueryPayOutKycValidation",
  "States": {
    "QueryPayOutKycValidation": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayOutKycValidationArn"},
      "ResultPath": "$.kycValidation",
      "Next": "CommandPayOutCryptoOrder"
    },
    "CommandPayOutCryptoOrder": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandPayOutCryptoOrderArn"},
      "ResultPath": "$.cryptoOrderTransactionPath",
      "Next": "QueryPayOutCryptoOrderStatus"
    },
    "QueryPayOutCryptoOrderStatus": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayOutCryptoOrderStatusArn"},
      "ResultPath": "$.cryptoOrder",
      "Next": "ChoiceCryptoOrderStatus"
    },
    "ChoiceCryptoOrderStatus": {
      "Type": "Choice",
      "Choices": [
        {
          "Variable": "$.cryptoOrder.status",
          "StringEquals": "DONE",
          "Next": "CommandAndQueryPayOutFeeAmount"
        }
      ],
      "Default": "WaitCryptoOrderStatus"
    },
    "WaitCryptoOrderStatus": {
      "Comment": "",
      "Type": "Wait",
      "Seconds": 60,
      "Next": "QueryPayOutCryptoOrderStatus"
    },
    "CommandAndQueryPayOutFeeAmount": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandAndQueryPayOutFeeAmountArn"},
      "ResultPath": "$.feeAmount",
      "Next": "QueryPayOutNetAmount"
    },
    "QueryPayOutNetAmount": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayOutNetAmountArn"},
      "ResultPath": "$.netAmount",
      "Next": "CommandPayOutPayPal"
    },
    "CommandPayOutPayPal": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandPayOutPayPalArn"},
      "ResultPath": "$.payout",
      "Next": "CommandPayOutTransaction"
    },
    "CommandPayOutTransaction": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandPayOutTransactionArn"},
      "End": true
    }
  }
}