// this is just for documentation purposes, real code is in PayIn-cloudformation-template.json, please keep in sync
{
  "StartAt": "QueryPayInKycValidation",
  "States": {
    "QueryPayInKycValidation": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayInKycValidationArn"},
      "ResultPath": "$.kycValidation",
      "Next": "QueryPayInPaymentStatus"
    },
    "QueryPayInPaymentStatus": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayInPaymentStatusArn"},
      "ResultPath": "$.paymentStatus",
      "Next": "ChoicePaymentStatus"
    },
    "ChoicePaymentStatus": {
      "Type": "Choice",
      "Choices": [
        {
          "Variable": "$.paymentStatus",
          "StringEquals": "DONE",
          "Next": "CommandAndQueryPayInFeeAmount"
        },
        {
          "Variable": "$.paymentStatus",
          "StringEquals": "FAILED",
          "Next": "UnhandledError"
        }
      ],
      "Default": "WaitPaymentStatus"
    },
    "WaitPaymentStatus": {
      "Comment": "",
      "Type": "Wait",
      "Seconds": 60,
      "Next": "QueryPayInPaymentStatus"
    },
    "CommandAndQueryPayInFeeAmount": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandAndQueryPayInFeeAmountArn"},
      "ResultPath": "$.feeAmount",
      "Next": "QueryPayInNetAmount"
    },
    "QueryPayInNetAmount": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayInNetAmountArn"},
      "ResultPath": "$.netAmount",
      "Next": "ChoiceNetAmountSet"
    },
    "ChoiceNetAmountSet": {
      "Type": "Choice",
      "Choices": [
        {
          "Variable": "$.netAmount.amount",
          "NumericGreaterThanEquals": 0,
          "Next": "CommandPayInCryptoOrder"
        }
      ],
      "Default": "WaitNetAmount"
    },
    "WaitNetAmount": {
      "Comment": "",
      "Type": "Wait",
      "Seconds": 60,
      "Next": "QueryPayInNetAmount"
    },
    "CommandPayInCryptoOrder": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandPayInCryptoOrderArn"},
      "ResultPath": "$.cryptoOrderTransactionPath",
      "Next": "QueryPayInCryptoOrderStatus"
    },
    "QueryPayInCryptoOrderStatus": {
      "Type": "Task",
      "Resource": {"Ref": "functionQueryPayInCryptoOrderStatusArn"},
      "ResultPath": "$.cryptoOrder",
      "Next": "ChoiceCryptoOrderStatus"
    },
    "ChoiceCryptoOrderStatus": {
      "Type": "Choice",
      "Choices": [
        {
          "Variable": "$.cryptoOrder.status",
          "StringEquals": "DONE",
          "Next": "CommandPayInTransaction"
        }
      ],
      "Default": "WaitCryptoOrderStatus"
    },
    "WaitCryptoOrderStatus": {
      "Comment": "",
      "Type": "Wait",
      "Seconds": 60,
      "Next": "QueryPayInCryptoOrderStatus"
    },
    "CommandPayInTransaction": {
      "Type": "Task",
      "Resource": {"Ref": "functionCommandPayInTransactionArn"},
      "End": true
    },
    "UnhandledError": {
        "Type": "Fail"
    }
  }
}