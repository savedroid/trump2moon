import "react-native"
// Note: test renderer must be required after react-native.
import {shallow} from "enzyme"
import React from "react"
import GetStartedScreen from "../../../src/screens/savingRule/getStartedScreen";
import BalanceView from "../../../src/screens/balance/balanceView";
import {Image, TouchableOpacity} from "react-native";
import renderer from "react-test-renderer";

describe("<GetStartedScreen>", () => {

    const wrapper = shallow(
        <GetStartedScreen/>
    );

    it("should render correctly",  () => {
        expect(wrapper.find(BalanceView).first().exists());
        expect(wrapper.find(Image).first().exists());
        expect(wrapper.find(TouchableOpacity).first().exists());

        const tree = renderer.create(<GetStartedScreen/>).toJSON();
        expect(tree).toMatchSnapshot();
        expect(wrapper).toMatchSnapshot();
    });

    it("values should be set properly ", function () {
        const balanceView = wrapper.find(BalanceView).first();
        expect(balanceView.prop("balance")).toEqual(0);
        expect(balanceView.prop("bitcoinBalance")).toEqual(0);
    });
});