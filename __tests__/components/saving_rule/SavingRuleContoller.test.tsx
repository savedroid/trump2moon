import "react-native"
// Note: test renderer must be required after react-native.
import {shallow} from "enzyme"
import React from "react"
import SavingRuleController, {ViewState} from "../../../src/screens/savingRule/controller";
import SavingRuleSuccess from "../../../src/screens/savingRule/success_screen";
import LoadingSpinner from "../../../src/components/loadingSpinner";
import CircleSizeSelector from "../../../src/components/customViews/amountPicker/CircleSizeSelector";

const InitialValue = 0.5;
const InitialTextSize = 20;

describe("<SavingRuleController>", () => {

    const wrapper = shallow(
        <SavingRuleController navigation={null}/>
    );

    it("renders as expected", () => {
        expect(wrapper.find(SavingRuleSuccess).first().exists());
        expect(wrapper.find(LoadingSpinner).first().exists());
        expect(wrapper.find(CircleSizeSelector).first().exists());

        expect(wrapper).toMatchSnapshot();
    });

    it('should have initial state', function () {
        expect(wrapper.state("amount")).toEqual(InitialValue);
        expect(wrapper.state("textSize")).toEqual(InitialTextSize);
        expect(wrapper.state("viewState")).toEqual(ViewState.SELECT_AMOUNT);
    });

    it('should set the viewStates', function () {
        wrapper.setState({viewState: ViewState.LOADING});
        expect(wrapper.state("viewState")).toEqual(ViewState.LOADING);

        wrapper.setState({viewState: ViewState.SUCCESS});
        expect(wrapper.state("viewState")).toEqual(ViewState.SUCCESS);

        wrapper.setState({viewState: ViewState.SELECT_AMOUNT});
        expect(wrapper.state("viewState")).toEqual(ViewState.SELECT_AMOUNT);
    });

    it('amount should be set properly ', function () {
        const selector = wrapper.find(CircleSizeSelector).first();
        expect(selector.prop("initialValue")).toEqual(InitialValue);
        expect(selector.prop("manualValues")).toBeDefined();
    });

    // xit('onChange should change the state',  () => {
    //     const spy = spyOn(wrapper.instance(), "onAmountChanged");
    //     wrapper.update();
    //     wrapper.find(CircleSizeSelector).first().simulate("change",
    //         { target: { value: 5}});
    //
    //   //  expect(spy).toHaveBeenCalled();
    // });
});