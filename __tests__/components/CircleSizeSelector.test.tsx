import "react-native";
// Note: test renderer must be required after react-native.
import renderer from "react-test-renderer";
import React from "react";
import CircleSizeSelector from "../../src/components/customViews/amountPicker/CircleSizeSelector";

describe("Circle size selector", () => {

    it("renders correctly", () => {
        const tree = renderer.create(<CircleSizeSelector  initialValue={1} minValue={5} maxValue={10}/>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
