import "react-native"
import React from "react"
import { shallow } from "enzyme"
import SignUpController from "../../src/screens/login/signUp/controller"

it("renders as expected", () => {
    const wrapper = shallow(
        <SignUpController navigation={null} />
    )

    wrapper.setState({ username: "test_user", email: "test@mail.com", password: "12345678", error: "an error occured" })
    expect(wrapper.state("username")).toEqual("test_user")
    expect(wrapper.state("email")).toEqual("test@mail.com")
    expect(wrapper.state("password")).toEqual("12345678")
    expect(wrapper.state("error")).toEqual("an error occured")

    expect(wrapper).toMatchSnapshot()
})
