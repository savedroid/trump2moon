import PaymentService from "../../../src/services/paymentService"
import {AppSync} from "../../../src/services/appsync";

jest.mock("../../../src/screens/paypal/paymentService")

describe("Testing payment function", () => {

    xit('should show the result', function () {
        const paymentService = new PaymentService(new AppSync())
        expect(PaymentService).toHaveBeenCalledTimes(1)
        return paymentService
            .startPayment(1.0)
            .then((result: boolean) => {
                expect(result).toEqual("false")
                console.log("result is: " + result)
            });
    });

});
