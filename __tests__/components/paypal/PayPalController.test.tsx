import "react-native"
// Note: test renderer must be required after react-native.
import {mount, shallow} from "enzyme"
import React from "react"
import PayPalController from "../../../src/screens/paypal/controller"

describe("<PayPalController>", () => {

    it("renders as expected", () => {
        const wrapper = shallow(
            <PayPalController navigation={null}/>
        )
        wrapper.setState({payPalSuccess: false})
        expect(wrapper.state("payPalSuccess")).toEqual(false)


        wrapper.setState({payPalSuccess: true})
        expect(wrapper.state("payPalSuccess")).toEqual(true)

        expect(wrapper).toMatchSnapshot()
    })
})
