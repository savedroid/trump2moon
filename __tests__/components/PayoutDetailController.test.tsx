import "react-native"
// Note: test renderer must be required after react-native.
import {shallow} from "enzyme"
import React from "react"
import PayoutDetailController from "../../src/screens/payout/details/controller";

describe("<PayoutDetail>", () => {

    it("renders as expected", () => {
        const wrapper = shallow(
            <PayoutDetailController navigation={null} balance={"10"} route={null}/>
        )
        expect(wrapper).toMatchSnapshot()
    })
})
