import "react-native"
// Note: test renderer must be required after react-native.
import {shallow} from "enzyme"
import React from "react"
import PayoutSummaryController from "../../src/screens/payout/summary/controller";

describe("<PayoutSummaryController>", () => {

    it("renders as expected", () => {
        const wrapper = shallow(
            <PayoutSummaryController navigation={null} amount={"10"} bankAccount={"dummy"}
                                     route={null}/>
        )
        wrapper.setState({amount: "10", bankAccount: "dummy"})

        expect(wrapper).toMatchSnapshot()
    })
})
