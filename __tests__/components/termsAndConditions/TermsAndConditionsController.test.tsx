import "react-native"
// Note: test renderer must be required after react-native.
import {shallow} from "enzyme"
import React from "react"

import TermsAndConditionsController from "../../../src/screens/login/termsAndConditions/controller"
import WebView from "react-native-webview";

describe("<TermsAndConditionsController>", () => {

    const wrapper = shallow(<TermsAndConditionsController navigation={null}/>)
    it('should find the WebView',  () => {
        expect(wrapper.find(WebView).dive().find("source")).toBeDefined()
        expect(wrapper.find(WebView).first().props().source).toHaveProperty("uri")
    });

    it("renders as expected", () => {
        expect(wrapper).toMatchSnapshot()
    });
});