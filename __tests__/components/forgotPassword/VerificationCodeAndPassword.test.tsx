import "react-native"
import React from "react"
import {shallow} from "enzyme"
import ForgotPasswordController from "../../../src/screens/login/forgotPassword/controller";

it("renders as expected", () => {
    const props = {
        route: {
            params: {
                username: ""
            }
        }
    }

    const wrapper = shallow(
        <ForgotPasswordController {...props} />
    )

    wrapper.setState({
        username: "test_user@savedroid.de",
        code: "12341234",
        password: "12345678",
        error: "An error occured!"
    })
    expect(wrapper.state("username")).toEqual("test_user@savedroid.de")
    expect(wrapper.state("code")).toEqual("12341234")
    expect(wrapper.state("password")).toEqual("12345678")
    expect(wrapper.state("error")).toEqual("An error occured!")

    expect(wrapper).toMatchSnapshot()
})
