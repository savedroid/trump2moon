import "react-native"
import React from "react"
import {shallow} from "enzyme"
import EmailController from "../../../src/screens/login/forgotPassword/email/controller";

it("renders as expected", () => {
    const wrapper = shallow(
        <EmailController navigation={null}/>
    )

    wrapper.setState({username: "test_user@savedroid.de", error: "an error occured"})
    expect(wrapper.state("username")).toEqual("test_user@savedroid.de")
    expect(wrapper.state("error")).toEqual("an error occured")

    expect(wrapper).toMatchSnapshot()
})