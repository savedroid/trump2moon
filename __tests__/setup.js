import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { NativeModules as RNNativeModules } from "react-native";
import { NativeModules } from "react-native";
RNNativeModules.UIManager = RNNativeModules.UIManager || {};
RNNativeModules.UIManager.RCTView = RNNativeModules.UIManager.RCTView || {};
RNNativeModules.RNGestureHandlerModule = RNNativeModules.RNGestureHandlerModule || {
    State: { BEGAN: "BEGAN", FAILED: "FAILED", ACTIVE: "ACTIVE", END: "END" },
};

global.fetch = require("jest-fetch-mock");
Enzyme.configure({ adapter: new Adapter() });

NativeModules.RNCNetInfo = {
    getCurrentState: jest.fn(() => Promise.resolve({})),
    addListener: jest.fn(),
    isConnected: {
        addEventListener: jest.fn(),
        fetch: () => Promise.resolve({}),
    },
    removeListeners: jest.fn(),
    getConnectionInfo: jest.fn(() => { Promise.resolve(); }),
};

// jest.mock('NetInfo', () => {
//     return {
//         isConnected: {
//             fetch: () => {
//                 return new Promise((accept, resolve) => {
//                     accept(true);
//                 });
//             },
//         },
//     };
// });