import "react-native";
import React from "react";
import {shallow} from "enzyme";
import BalanceController from "../src/screens/balance/controller";

it("renders as expected", () => {
    const wrapper = shallow(
        <BalanceController navigation={null} route={null}/>
    )
    wrapper.setState({pendingAmount: "10", balance: "100", bitcoinBalance: "0.4567", tweetAmount: "5"})
    expect(wrapper.state("pendingAmount")).toEqual("10")

    expect(wrapper).toMatchSnapshot()
})
