/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import "react-native-gesture-handler";
import React from "react";
import {StatusBar} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import {Event} from "./src/helpers/Subscriptions";
import {CognitoService} from "./src/services/cognito";
import SplashScreenController from "./src/screens/splashscreen/controller";
import {RootStackScreen} from "./src/screens/navigation";
import Icon from "react-native-vector-icons/FontAwesome";
import { MigrationService } from "./src/services/migration";

interface State {
    sessionValid : boolean
}

Icon.loadFont();

export enum SessionState {
    LoggedIn,
    New,
    ExistingUser
}

export default function() {
    const [sessionState, setSessionState] = React.useState(SessionState.New);
    const [isLoading, setIsLoading] = React.useState(true);
    const cognito = CognitoService.shared;
    const migrationService = new MigrationService();

    const on = (event: Event) => {
        console.log(event);
        switch (event.type) {
        case "noSessionAvailable" : setSessionState(SessionState.New); break;
        case "sessionEstablished" : setSessionState(SessionState.LoggedIn); break;
        }
    };

    cognito.getSession()
        .then(() => setSessionState(SessionState.LoggedIn))
        .catch(async () => { 
            const email = await migrationService.migrate();
            setSessionState(email ? SessionState.ExistingUser : SessionState.New);
        } )
        .finally(() => setIsLoading(false));

    cognito.subscribeWith(on);

    if (isLoading) {
        return <SplashScreenController />;
    }

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <NavigationContainer >
                <RootStackScreen sessionState={sessionState} />
            </NavigationContainer>
        </>
    );
}
