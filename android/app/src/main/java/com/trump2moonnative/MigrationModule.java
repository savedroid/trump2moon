package com.trump2moonnative;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.trump2moonnative.crypto.SdAPI18EncryptionProvider;
import com.trump2moonnative.crypto.SdAPI23HardwareBackedEncryptionProvider;
import com.trump2moonnative.crypto.SdAPI23SecureSharedPrefsBackedEncryptionProvider;
import com.trump2moonnative.crypto.SdCipherFactory;
import com.trump2moonnative.crypto.SdEncryptionProvider;
import com.trump2moonnative.crypto.SdEncryptionProviderComposite;
import com.trump2moonnative.crypto.SdSecureSharedPreferences;

import androidx.annotation.NonNull;

public class MigrationModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    private SdSecureSharedPreferences preferences;

    public MigrationModule(@NonNull ReactApplicationContext context) {
        super(context);
        reactContext = context;
        SharedPreferences preferences = context.getSharedPreferences("global", Context.MODE_PRIVATE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.MANUFACTURER.toUpperCase().contains("HUAWEI")) {
            this.preferences = new SdSecureSharedPreferences(preferences, new SdCipherFactory(new SdAPI23SecureSharedPrefsBackedEncryptionProvider(context, preferences)));
        }

        // On Android M and up we first try to use the shared preferences backed encryption provider
        // This is less secure than the hardware backed provider, but using hardware backed encryption
        // causes many errors. On some devices the shared prefs backed provider fails, so we fallback
        // to the hardware backed provider
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SdEncryptionProvider provider = new SdEncryptionProviderComposite(
                    "standard",
                    preferences,
                    new SdAPI23SecureSharedPrefsBackedEncryptionProvider(context, preferences),
                    new SdAPI23HardwareBackedEncryptionProvider());
            this.preferences = new SdSecureSharedPreferences(preferences, new SdCipherFactory(provider));

        }

        // Everything easy on 18 to 22...
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            SdEncryptionProvider provider = new SdEncryptionProviderComposite(
                    "standard",
                    preferences,
                    new SdAPI18EncryptionProvider(context, preferences));
            this.preferences = new SdSecureSharedPreferences(preferences, new SdCipherFactory(provider));

        }

    }

    @NonNull
    @Override
    public String getName() {
        return "MigrationManager";
    }

    @ReactMethod
    public void migrateExistingUser(Promise promise) {
        promise.resolve(this.preferences.getString("email", null));

    }
}
