package com.trump2moonnative.crypto;

import android.content.SharedPreferences;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;


/**
 * A composite of {@link SdEncryptionProvider}s. When calling init the best provider which works on
 * this device is picked. Providers which a lower index are preferred.
 */
public class SdEncryptionProviderComposite extends SdEncryptionProvider {

    /**
     * All providers available
     */
    private SdEncryptionProvider[] mProviders;

    /**
     * The provider which is used
     */
    private SdEncryptionProvider mUsedProvider;

    /**
     * The name of this encryption provider
     */
    private String mName;

    /**
     * The SharedPreferences used to store the used provider
     */
    private SharedPreferences mPreferences;

    /**
     * Creates a new instance
     *
     * @param providers all providers
     */
    public SdEncryptionProviderComposite(String name, SharedPreferences preferences, SdEncryptionProvider... providers) {
        this.mProviders = providers;
        this.mName = name;
        this.mPreferences = preferences;

    }

    @Override
    public void init() throws Exception {
        // Only init if not done before
        if (this.mUsedProvider == null) {
            try {
                // Check if we have decided to use a provider before
                // If so, search for it and use it again
                String providerKey = "use_encryption_provider_" + this.mName;
                String providerClassName = this.mPreferences.getString(providerKey, null);
                if (providerClassName != null) {
                    for (SdEncryptionProvider provider : this.mProviders) {
                        if (provider.getClass().getName().equals(providerClassName)) {
                            this.mUsedProvider = provider;
                            this.mUsedProvider.init();
                            return;

                        }
                    }
                }


                // Iterate over all provider and init and test them. Use first provider which passes test
                for (SdEncryptionProvider provider : this.mProviders) {
                    try {
                        provider.init();
                        if (provider.test()) {
                            this.mUsedProvider = provider;
                            this.mPreferences.edit().putString(providerKey, this.mUsedProvider.getClass().getName()).apply();

                            return;

                        }
                    } catch (Exception e) {
                        // Do not log. At this point of time, we don't care why we can't use a certain provider
                        // TODO log

                    }
                }

                // We did not find a provider, throw exception
                throw new IllegalStateException("No provider is in usable shape");

            } finally {
                if (this.mUsedProvider != null) {


                }
            }
        }
    }

    /**
     * Checks whether init was called
     */
    private void checkInit() throws IllegalStateException {
        if (this.mUsedProvider == null) {
            throw new IllegalStateException("Call init() first");

        }
    }

    @Override
    public Key getSecretKey() throws Exception {
        this.checkInit();
        return this.mUsedProvider.getSecretKey();

    }

    @Override
    public AlgorithmParameterSpec getIvParameterSpec() throws Exception {
        this.checkInit();
        return this.mUsedProvider.getIvParameterSpec();

    }

    @Override
    protected byte[] getKey() throws Exception {
        this.checkInit();
        return this.mUsedProvider.getKey();

    }

    @Override
    protected byte[] getInitVector() throws Exception {
        this.checkInit();
        return this.mUsedProvider.getInitVector();

    }

    @Override
    public String getTransformation() {
        if (this.mUsedProvider != null) {
            return this.mUsedProvider.getTransformation();

        } else {
            return "{{uninitialised}}";

        }
    }
}
