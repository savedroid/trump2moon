package com.trump2moonnative.crypto;

import android.content.SharedPreferences;
import android.util.Base64;

import java.util.Map;
import java.util.Set;

import javax.crypto.Cipher;

import androidx.annotation.Nullable;

public class SdSecureSharedPreferences implements SharedPreferences {

    /**
     * The charset used for the strings
     */
    private static final String CHARSET = "UTF-8";
    /**
     * A value to replace null
     */
    private static final String NULL_VALUE = "randomnullvalue";
    /**
     * The wrapped {@link SharedPreferences}
     */
    private SharedPreferences mWrapped;
    /**
     * The {@link Encrypter} to encrypt Strings
     */
    private Encrypter mEncrypt;
    /**
     * The {@link SdCipherFactory}
     */
    private SdCipherFactory mCipherFactory;

    /**
     * Creates a new instance
     *
     * @param wrapped the wrapped {@link SharedPreferences}
     * @param factory the factory used to create the {@link Cipher}s
     */
    public SdSecureSharedPreferences(SharedPreferences wrapped, SdCipherFactory factory) {
        this.mWrapped = wrapped;
        this.mCipherFactory = factory;
        this.mEncrypt = new Encrypter(this.mCipherFactory);

    }

    @Override
    public Map<String, ?> getAll() {
        throw new IllegalStateException("Not implemented");

    }

    @Nullable
    @Override
    public String getString(String key, String defaultValue) {
        return this.decryptValue(this.mEncrypt.encrypt(key), defaultValue);

    }

    @Nullable
    @Override
    public Set<String> getStringSet(String s, Set<String> set) {
        throw new IllegalStateException("Not implemented");

    }

    @Override
    public int getInt(String key, int defaultValue) {
        return Integer.valueOf(this.getString(key, String.valueOf(defaultValue)));

    }

    @Override
    public long getLong(String key, long defaultValue) {
        return Long.valueOf(this.getString(key, String.valueOf(defaultValue)));

    }

    @Override
    public float getFloat(String key, float defaultValue) {
        return Float.valueOf(this.getString(key, String.valueOf(defaultValue)));

    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        return Boolean.valueOf(this.getString(key, String.valueOf(defaultValue)));

    }

    @Override
    public boolean contains(String key) {
        return this.mWrapped.contains(this.mEncrypt.encrypt(key));

    }

    @Override
    public Editor edit() {
        return new Editor(this.mWrapped.edit(), this.mEncrypt);

    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.mWrapped.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.mWrapped.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);

    }

    /**
     * Decrypts the given value from the {@link SharedPreferences} wrapped. Returns the given default
     * value if decryption fails or the value is not contained in the wrapped instance
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the value or the defaut value
     */
    private String decryptValue(String key, String defaultValue) {
        // Create Cipher. We cannot reuse a Cipher, as one Cipher can not be fed with
        // the same IV twice on Android M and up
        Cipher cipher;
        try {
            cipher = this.mCipherFactory.createCipher(SdCipherFactory.Mode.DECRYPT);

        } catch (Exception e) {
            throw new RuntimeException(e);

        }

        // Get value
        String value = this.mWrapped.getString(key, defaultValue);

        // Return null if value is null
        if(value == null) {
            return null;
        }

        // Return default value if default was returned from wraped (key not contained)
        if(value.equals(defaultValue)) {
            return defaultValue;
        }

        try {
            // Decrypt
            String s =  new String(cipher.doFinal(Base64.decode(value, Base64.NO_WRAP)), CHARSET);

            // If the value is the null value, return null
            if(NULL_VALUE.equals(s)) {
                return null;
            }

            return s;

        } catch (Exception e) {
            // Log and return default value
            return defaultValue;

        }
    }

    /**
     * A class wrapping a {@link Cipher} to decrypt values
     */
    private static class Encrypter {

        /**
         * The {@link SdCipherFactory}
         */
        private SdCipherFactory mCipherFactory;

        /**
         * Creates a new instance
         *
         * @param factory the {@link SdCipherFactory} used
         */
        Encrypter(SdCipherFactory factory) {
            this.mCipherFactory = factory;

        }

        /**
         * Encrypts the given value
         *
         * @param value the value
         * @return the encrypted value
         */
        String encrypt(String value)  {
            try {
                // Create Cipher. We cannot reuse a Cipher, as one Cipher can not be fed with
                // the same IV twice on Android M and up
                Cipher cipher = this.mCipherFactory.createCipher(SdCipherFactory.Mode.ENCRYPT);

                // Create cipher
                if(cipher == null) {
                    try {
                        cipher = this.mCipherFactory.createCipher(SdCipherFactory.Mode.ENCRYPT);
                    } catch (Exception e) {
                        throw new RuntimeException(e);

                    }
                }

                // Retrun null if value is null
                if(value == null) {
                    value = NULL_VALUE;
                }

                return Base64.encodeToString(cipher.doFinal(value.getBytes(CHARSET)), Base64.NO_WRAP);

            } catch (Exception e) {
                return "";
            }
        }
    }

    /**
     * A {@link android.content.SharedPreferences.Editor} wrapping an other {@link android.content.SharedPreferences.Editor} and
     * encrypting all values befor passing them to the wrapped instance
     */
    public static class Editor implements SharedPreferences.Editor {

        /**
         * The wrapped {@link android.content.SharedPreferences.Editor}
         */
        private SharedPreferences.Editor mWrapped;

        /**
         * The {@link Encrypter} used to encrypt values
         */
        private Encrypter mEncrypt;

        /**
         * Creates a new instance
         *
         * @param wrapped the {@link android.content.SharedPreferences.Editor} wrapped
         * @param encrypt the {@link Encrypter} used to encrypt the values and keys
         */
        public Editor(SharedPreferences.Editor wrapped, Encrypter encrypt) {
            this.mWrapped = wrapped;
            this.mEncrypt = encrypt;

        }

        @Override
        public SharedPreferences.Editor putString(String key, String value) {
            return this.mWrapped.putString(this.mEncrypt.encrypt(key), this.mEncrypt.encrypt(value));

        }

        @Override
        public SharedPreferences.Editor putStringSet(String s, Set<String> set) {
            throw new IllegalStateException("Not implemented");

        }

        @Override
        public SharedPreferences.Editor putInt(String key, int value) {
            return this.mWrapped.putString(this.mEncrypt.encrypt(key), this.mEncrypt.encrypt(String.valueOf(value)));

        }

        @Override
        public SharedPreferences.Editor putLong(String key, long value) {
            return this.mWrapped.putString(this.mEncrypt.encrypt(key), this.mEncrypt.encrypt(String.valueOf(value)));

        }

        @Override
        public SharedPreferences.Editor putFloat(String key, float value) {
            return this.mWrapped.putString(this.mEncrypt.encrypt(key), this.mEncrypt.encrypt(String.valueOf(value)));

        }

        @Override
        public SharedPreferences.Editor putBoolean(String key, boolean value) {
            return this.mWrapped.putString(this.mEncrypt.encrypt(key), this.mEncrypt.encrypt(String.valueOf(value)));

        }

        @Override
        public SharedPreferences.Editor remove(String key) {
            return this.mWrapped.remove(this.mEncrypt.encrypt(key));

        }

        @Override
        public SharedPreferences.Editor clear() {
            return this.mWrapped.clear();

        }

        @Override
        public boolean commit() {
            return this.mWrapped.commit();

        }

        @Override
        public void apply() {
            this.mWrapped.apply();

        }
    }
}