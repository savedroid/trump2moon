package com.trump2moonnative.crypto;

import android.annotation.TargetApi;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import com.trump2moonnative.crypto.SdEncryptionProvider;


import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.GCMParameterSpec;


/**
 * A {@link SdEncryptionProvider} which creates and manages a key using the AndroidKeyStore API. This
 * key is securely genrated and securely stored. The key generated can only be used for AES/CBC/PKCS7Padding.
 */
@TargetApi(Build.VERSION_CODES.M)
public class SdAPI23HardwareBackedEncryptionProvider extends SdEncryptionProvider {

    /**
     * The name of the Android keystore
     */
    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";

    /**
     * The alias for the generated key
     */
    private static final String KEY_ALIAS = "aes_key";

    /**
     * The keystore
     */
    private KeyStore mKeyStore;

    /**
     * Initialises the {@link #mKeyStore} if not already done. If the keystore is used the first time,
     * a new random key is generated
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws NoSuchProviderException
     * @throws InvalidAlgorithmParameterException
     */
    @Override
    public synchronized void init() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, NoSuchProviderException, InvalidAlgorithmParameterException {
        // Load key store ´
        if(this.mKeyStore == null) {
            this.mKeyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            this.mKeyStore.load(null);

            // If the keystore does not contain our key, create a new one
            if (!this.mKeyStore.containsAlias(KEY_ALIAS)) {

                // Build KeyGenerator
                KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);
                keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_ALIAS,
                        KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                        .setRandomizedEncryptionRequired(false)
                        .build());

                // Generate the key
                keyGenerator.generateKey();

            }
        }
    }

    @Override
    public synchronized Key getSecretKey() throws Exception {
        // We cannot export the key to raw bytes, so we return it directly from the keystore
        return this.mKeyStore.getKey(KEY_ALIAS, null);

    }

    @Override
    public AlgorithmParameterSpec getIvParameterSpec() throws Exception {
        // "Because of the security provided by Android KeyStore, a random IV is an overkill here is so I use a fixed IV instead"
        // https://medium.com/@ericfu/securely-storing-secrets-in-an-android-application-501f030ae5a3#.mic61q4lw
        return new GCMParameterSpec(128, "2063dfl[$-=2".getBytes());

    }

    @Override
    public String getTransformation() {
        return "AES/GCM/NoPadding";

    }

    @Override
    protected byte[] getKey() {
        // Never called
        return null;

    }

    @Override
    protected byte[] getInitVector() {
        // Never called
        return null;

    }
}
