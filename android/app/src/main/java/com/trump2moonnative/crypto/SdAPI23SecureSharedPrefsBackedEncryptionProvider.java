package com.trump2moonnative.crypto;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

/**
 * A {@link SdEncryptionProvider} which creates and manages a key using the AndroidKeyStore API. This
 * key is securely genrated and securely stored. The key generated can only be used for AES/CBC/PKCS7Padding.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class SdAPI23SecureSharedPrefsBackedEncryptionProvider extends SdAPI18EncryptionProvider {

    /**
     * Creates a new instance
     *
     * @param con         a {@link Context}
     * @param preferences the {@link SharedPreferences} used to store the AES key
     */
    public SdAPI23SecureSharedPrefsBackedEncryptionProvider(Context con, SharedPreferences preferences) {
        super(con, preferences);

    }

    @Override
    protected Cipher createCipher(int mode, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        Cipher output = Cipher.getInstance(RSA_MODE);
        output.init(mode, key);
        return output;

    }
}
