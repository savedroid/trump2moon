package com.trump2moonnative.crypto;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * A abstract super class for encryption key factories creating AES keys
 */
public abstract class SdEncryptionProvider {

    /**
     * Returns the encryption key based on the device info
     *
     * @return encryption key
     */
    public Key getSecretKey() throws Exception {
        // Get key and verify length
        byte[] key = this.getKey();

        // Create and return Key
        return new SecretKeySpec(key, "AES");

    }

    /**
     * Tests the provider
     */
    public boolean test() {
        try {
            // Encrypt and decrypt a String to test encryption. Test is passed if decrypted value
            // equals raw value and no Exception was thrown
            Cipher c = Cipher.getInstance(this.getTransformation());
            c.init(Cipher.ENCRYPT_MODE, this.getSecretKey(), this.getIvParameterSpec());
            String rawData = "savedroidRocks";
            byte[] encrypted = c.doFinal(rawData.getBytes());
            c.init(Cipher.DECRYPT_MODE, this.getSecretKey(), this.getIvParameterSpec());
            String decrypted = new String(c.doFinal(encrypted));
            return rawData.equals(decrypted);

        } catch (Exception e) {
            return false;

        }
    }

    /**
     * Initialises the provider. Must be called before {@link #getKey()} or {@link #getInitVector()}.
     * This method should check if it was invoked before.
     */
    public abstract void init() throws Exception;

    /**
     * Returns the key used for encryption
     * @return the key used for encryption
     */
    protected abstract byte[] getKey() throws Exception;

    /**
     * Returns the init vector used to create the {@link IvParameterSpec}. This must be 16 bytes.
     * @return the init vector used to create the {@link IvParameterSpec}
     */
    protected abstract byte[] getInitVector() throws Exception;


    /**
     * Returns the encryption key based on the device info
     *
     * @return encryption key
     */
    public AlgorithmParameterSpec getIvParameterSpec() throws Exception {
        return new IvParameterSpec(this.getInitVector());

    }

    /**
     * Returns the algorithm which is used by this factory
     * @return the algorithm, e.g. "AES/CBC/PKCS7Padding"
     */
    public abstract String getTransformation();

}
