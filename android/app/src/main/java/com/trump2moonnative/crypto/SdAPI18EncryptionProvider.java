package com.trump2moonnative.crypto;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

/**
 * Provides device-specific encryption key and a standard init vector for AES encryption
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class SdAPI18EncryptionProvider extends SdEncryptionProvider {

    /**
     * The RSA transformation used to encrypt/decrypt the AES key
     */
    protected static final String RSA_MODE = "RSA/ECB/PKCS1Padding";
    /**
     * The name of the Android keystore
     */
    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";
    /**
     * The key for the AES key in the preferences
     */
    private static final String ENCRYPTED_KEY = "aes_key";

    /**
     * The alias for the generated key
     */
    private static final String RSA_KEY_ALIAS = "rsa_key";

    /**
     * The {@link SharedPreferences} used to store the AES key
     */
    private SharedPreferences mPreferences;

    /**
     * The {@link Context}
     */
    private Context mContext;

    /**
     * The {@link KeyStore} used to store the RSA public/private keys
     */
    private KeyStore mKeyStore;

    /**
     * The AES key read from the preferences and decrypted using the RSA private key
     */
    private Key mKey;

    /**
     * Creates a new instance
     * @param con a {@link Context}
     * @param preferences the {@link SharedPreferences} used to store the AES key
     */
    public SdAPI18EncryptionProvider(Context con, SharedPreferences preferences) {
        this.mPreferences = preferences;
        this.mContext = con;

    }

    /**
     * Initialises the {@link #mKey} if not already done. If the keystore is used the first time,
     * a new random key is generated
     */
    @Override
    public synchronized void init() throws Exception {
        // Check if initialised before
        if (this.mKey == null) {
            // Load key store ´
            this.mKeyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            this.mKeyStore.load(null);

            // Generate the RSA key pairs if not available
            if (!this.mKeyStore.containsAlias(RSA_KEY_ALIAS)) {
                // Generate a key pair for encryption (valid for 30 years)
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                end.add(Calendar.YEAR, 30);

                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(this.mContext)
                        .setAlias(RSA_KEY_ALIAS)
                        .setSubject(new X500Principal("CN=" + RSA_KEY_ALIAS))
                        .setSerialNumber(BigInteger.TEN)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();

                KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", ANDROID_KEY_STORE);
                kpg.initialize(spec);
                kpg.generateKeyPair();

            }

            // Read key from preferences
            String enryptedKeyB64 = this.mPreferences.getString(ENCRYPTED_KEY, null);

            // If we have no key generated yet, genrated a key
            if (enryptedKeyB64 == null) {
                byte[] key = new byte[16];
                SecureRandom secureRandom = new SecureRandom();
                secureRandom.nextBytes(key);
                byte[] encryptedKey = this.rsaEncrypt(key);
                enryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
                SharedPreferences.Editor edit = this.mPreferences.edit();
                edit.putString(ENCRYPTED_KEY, enryptedKeyB64);
                edit.apply();

            }

            // Decrypt the key read
            byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
            byte[] key = rsaDecrypt(encryptedKey);

            // Generate SecretKeySpec
            this.mKey = new SecretKeySpec(key, "AES");

        }
    }

    /**
     * Encrypts the given bytes with the RSA private key
     *
     * @param data the raw data
     * @return the encrypted data
     * @throws Exception if decryption fails
     */
    private byte[] rsaEncrypt(byte[] data) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) this.mKeyStore.getEntry(RSA_KEY_ALIAS, null);
        Cipher inputCipher = this.createCipher(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(data);
        cipherOutputStream.close();

        return outputStream.toByteArray();

    }

    /**
     * Decrypts the given bytes with the RSA public key
     *
     * @param encrypted the encrypted data
     * @return the decrypted data
     * @throws Exception if decryption fails
     */
    private byte[] rsaDecrypt(byte[] encrypted) throws Exception {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) this.mKeyStore.getEntry(RSA_KEY_ALIAS, null);
            Cipher output = this.createCipher(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
            CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(encrypted), output);
            ArrayList<Byte> values = new ArrayList<>();
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                values.add((byte) nextByte);
            }

            byte[] bytes = new byte[values.size()];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = values.get(i);
            }
            return bytes;

        } catch (NullPointerException e) {

            throw e;

        }
    }

    /**
     * Creates a {@link Cipher}
     *
     * @param mode the {@link Cipher} mode
     * @param key  the {@link Key} to be used
     * @return the initialised {@link Cipher}
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     */
    protected Cipher createCipher(int mode, Key key) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(mode, key);
        return output;

    }

    @Override
    public synchronized Key getSecretKey() throws Exception {
        // Return the key
        return this.mKey;

    }

    @Override
    protected byte[] getKey() {
        // Never called
        return null;

    }

    @Override
    protected byte[] getInitVector() {
        return "GcMfQ7yj9qmfFz==".getBytes();

    }

    @Override
    public String getTransformation() {
        return "AES/CBC/PKCS7Padding";

    }
}
