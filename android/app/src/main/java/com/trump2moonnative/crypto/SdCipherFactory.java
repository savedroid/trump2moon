package com.trump2moonnative.crypto;

import javax.crypto.Cipher;

public class SdCipherFactory {

    /**
     * Encryption key factory
     */
    private SdEncryptionProvider mKeyFactory;

    /**
     * Creates a new instance with the given {@link SdEncryptionProvider}
     *
     * @param keyFactory     the {@link SdEncryptionProvider} holding the key used for encryption
     */
    public SdCipherFactory(SdEncryptionProvider keyFactory) {
        this.mKeyFactory = keyFactory;

    }

    @Override
    public String toString() {
        return super.toString() + "[transformation=" + this.mKeyFactory.getTransformation() + ", provider=" + this.mKeyFactory.getClass().getSimpleName() + "]";

    }

    /**
     * Returns the cipher based on encryption key
     *
     * @return the cipher
     *
     */
    public synchronized Cipher createCipher(Mode mode) throws Exception {
        try {
            // Init keys
            this.mKeyFactory.init();

            // Creating Cipher, use default provider
            Cipher cipher;
            cipher = Cipher.getInstance(this.mKeyFactory.getTransformation());

            // Initialising Cipher
            this.initCipher(cipher, mode == Mode.DECRYPT ? Cipher.DECRYPT_MODE : Cipher.ENCRYPT_MODE);

            // Return the configured Cipher
            return cipher;

        } catch (Exception e) {
            throw new Exception(e);

        }
    }

    /**
     * Returns the {@link SdEncryptionProvider} which powers this facotry
     *
     * @return the {@link SdEncryptionProvider}
     */
    protected SdEncryptionProvider getEncryptionProvider() {
        return this.mKeyFactory;

    }

    /**
     * Initialises the {@link Cipher} in the given mode
     *
     * @param cipher the  {@link Cipher} to initialise
     * @param mode   the mode
     */
    protected void initCipher(Cipher cipher, int mode) throws Exception {
        cipher.init(
                mode,
                this.mKeyFactory.getSecretKey(),
                this.mKeyFactory.getIvParameterSpec());

    }

    /**
     * A enum holding the modes
     */
    public enum Mode {
        ENCRYPT, DECRYPT
    }
}